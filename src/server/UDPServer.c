#include "UDPServer.h"

/* --- IMPORTS --- */
/* Common */
#include <stdlib.h>
#include <stdio.h>
#include "globals.h"
/* Networking */
#include "network_api.h"
#include "tnfs_UDP_packet.h"
#include "connection.h"
#include "transmission.h"
#include "UDP_procedures.h"
/* Multithreading */
#include "multithreading_api.h"
/* Utilitaries */
#include "uerror.h"
#include <time.h>
#include "timeout.h"

/* --- CODE --- */

#define DELAY 60000

void handle_tnfs_UDP_packet(void* args) {
    t_tnfs_UDP_packet* packet = args;

    switch(packet->type) {
        case F_TNFS_PING: handle_ping_procedure(packet); break;
        case F_TNFS_FIND_FILE: handle_find_file_procedure(packet); break;
        case F_TNFS_FIND_FILE_RESPONSE: handle_find_file_reponse_procedure(packet); break;
    }
    destroy_tnfs_UDP_packet(packet);
}

int start_UDP_server() {
    const t_socket_fd socket_fd = host_UDP_server(G_port_num); 

    if (socket_fd == INVALID_SOCKET_FD) {
        error_message("server hosting failed, shutting down TNFS...");
        kill_thread(G_UDP_server_thread);
        if (G_client_mode) kill_thread(G_client_thread);
        error_exit(NULL);
    }

    fprintf(stdout, "Local UDP server listening on port %d.\n", G_port_num);
    while(1) {
        t_socket_fd client_socket_fd; t_tnfs_UDP_packet* client_packet;
        if (read_tnfs_UDP_packet(socket_fd, &client_packet) == REQUEST_SUCESS) create_new_thread(handle_tnfs_UDP_packet, client_packet);   
    }

    return EXIT_SUCCESS;
}