#include "TCPServer.h"

/* --- IMPORTS --- */
/* Common */
#include <stdlib.h>
#include <stdio.h>
#include "globals.h"
/* Networking */
#include <sys/types.h>
#include <sys/select.h>
#include "network_api.h"
#include "connection.h"
#include "transmission.h"
/* Request */
#include "TCP_request_api.h"
/* Multithreading */
#include "multithreading_api.h"
/* Utilitaries */
#include "hash32.h"
#include <string.h>
#include "uerror.h"
#include <time.h>
#include "timeout.h"
#include <string.h>

/* --- CODE --- */

#define DELAY 60000

void handle_client(void* args) {
    t_socket_fd client_socket_fd;
    memcpy(&client_socket_fd, args, sizeof(t_socket_fd));
    fprintf(stdout, "[CLIENT %d] connection accepted.\n", client_socket_fd);
    
    hash32_t dest_node = handle_initiate_TCP_request(client_socket_fd);

    t_tnfs_TCP_packet* procedure_packet;
    read_tnfs_TCP_packet(client_socket_fd, &procedure_packet);

    const t_tnfs_packet_type type = procedure_packet->type;
    const void* payload = procedure_packet->payload;
    if (type == F_TNFS_DOWNLOAD) {
        fprintf(stdout, "[CLIENT %d] started a download procedure.\n", client_socket_fd);
        handle_download_file_request(client_socket_fd, dest_node, G_local_node_id, *((hash32_t*) payload));
        fprintf(stdout, "[CLIENT %d] download procedure ended with success.\n", client_socket_fd);
    } else if (type == F_TNFS_PUBLISH) {
        fprintf(stdout, "[CLIENT %d] started a publish procedure.\n", client_socket_fd);
        handle_file_publishing_request(client_socket_fd, dest_node, G_local_node_id, (char*) payload, procedure_packet->payload_size);
        fprintf(stdout, "[CLIENT %d] publish procedure ended with success.\n", client_socket_fd);
    } else if (type == F_TNFS_GET_PEERS) {
        fprintf(stdout, "[CLIENT %d] started a peers sharing procedure.\n", client_socket_fd);
        handle_get_peers_request(client_socket_fd, dest_node);
        fprintf(stdout, "[CLIENT %d] peers sharing procedure ended with success.\n", client_socket_fd);
    }


    destroy_tnfs_TCP_packet(procedure_packet);

    handle_end_TCP_request(client_socket_fd);
}

void error_TCP_server(char* message) {
    error_message(message);
    kill_thread(G_UDP_server_thread);
    if (G_client_mode) kill_thread(G_client_thread);
    error_exit(NULL);
}

int start_TCP_server() {
    const t_socket_fd tcp_server_socket_fd = host_TCP_server(G_port_num); 

    if (tcp_server_socket_fd == INVALID_SOCKET_FD) error_TCP_server("server hosting failed, shutting down TNFS...");
    if (listen(tcp_server_socket_fd, G_maximum_number_of_client) < 0) error_TCP_server("server listening failed, shutting down TNFS...");
    
    fprintf(stdout, "Local TCP server listening on port %d.\n", G_port_num);
    
    while(1) {
        t_socket_fd client_socket_fd; t_socket_config client_config; int sizeof_client_config;
        client_socket_fd = accept(tcp_server_socket_fd, (struct sockaddr*) &client_config, &sizeof_client_config);
        if (client_socket_fd == INVALID_SOCKET_FD) error_TCP_server("server accept failed, shutting down TNFS...");
        create_new_thread(handle_client, (void*) &client_socket_fd);   
    }

    close_socket(tcp_server_socket_fd);
    return EXIT_SUCCESS;
}