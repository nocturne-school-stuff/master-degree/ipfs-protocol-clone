/*
 * This file is a test area where you can
 * test your functions and modules.
 */

#include "test.h"

/* --- IMPORTS --- */
/* Common */
#include <stdlib.h>
/* Testing purposes */

#include "transmission.h"
#include "hash32.h"
#include "tnfs_TCP_packet.h"
#include "fs_api.h"
#include "serialization.h"

#include "local_config_api.h"

#include "serialization_files.h"
#include "filesDAO.h"
#include "peersDAO.h"
#include "ustring.h"
#include <stdio.h>
#include <string.h>
#include <ustring.h>
#include "random.h"
#include "kbucket.h"
#include "math.h"
#include "globals.h"
/* --- CODE --- */
int start_test_playground() {
    init_random_seed();
    int number_of_peers = random_int(10, 20);
    printf("Starting test with %d peers...\n", number_of_peers);
    Peer** peers = malloc(sizeof(Peer*) * number_of_peers);
    for (int i = 0; i < number_of_peers; i++) {
        int fake_length_ptr = 0;
        peers[i] = malloc(sizeof(Peer));
        strncpy(peers[i]->peer_id, hash32_to_string(random_hash32()), 5);
        strncpy(peers[i]->peer_ip_address, random_string(&fake_length_ptr, 10,10), 16);
        peers[i]->peer_source_port = random_int(1024, 3945);
    }

    hash32_t random_hash = random_hash32(); printf("Random hash searching nearest : "); print_hash(random_hash); printf(" distance : %ld\n", xor_metric(G_local_node_id, random_hash));

    BucketRow* originalBucket = init_bucket_row(0, pow(2, CID_LENGTH * 8) - 1);
    for (int i = 0; i < number_of_peers; i++) {
        printf("PEER : "); print_hash(string_to_hash32(peers[i]->peer_id)); printf("ip_addr: %s distance : %ld\n", peers[i]->peer_ip_address, xor_metric(random_hash, string_to_hash32(peers[i]->peer_id)));
        add_peer_to_bucket_row(originalBucket, peers[i]);

    }
    fprintf(stdout, "ALL PEARS ADDED IN BUCKETS YEHAH!\n");

    Peer* searched_peers = find_nearest_peer(originalBucket, random_hash);
    if (searched_peers == NULL) fprintf(stdout, "Could find peers :((((\n");
    else fprintf(stdout, "test peer ip addr = %s, distance from hash: %ld\n", searched_peers->peer_ip_address, xor_metric(random_hash, string_to_hash32(searched_peers->peer_id)));

    return EXIT_SUCCESS;
}
