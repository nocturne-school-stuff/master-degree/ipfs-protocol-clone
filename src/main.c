/* --- IMPORTS --- */
/* Common */
#include <stdlib.h>
#include <stdio.h>
/* Arguments parsing */
#include "ustring.h"
#include "globals.h"
/* Client & Server */
#include "client.h" 
#include "TCPServer.h"
#include "UDPServer.h"
#include "local_config_api.h"
/* Networking */
#include "kbucket.h"
#include "peersDAO.h"
#include "tnfs_TCP_packet.h"
/* Multithreading */
#include  "multithreading_api.h"
/* Utilitaries */
#include "uerror.h" 
#include "random.h"
#include <sqlite3.h>
#include "fs_api.h"
#include <math.h>
/* Testing */
#include "test.h"

int main(int argc, char* argv[]) {
    if (1 < argc && str_equals(argv[1], "--test-mode")) {
        fprintf(stdout, "Starting the TNFS test playground...\n");
        const int test_result = start_test_playground();
        if (test_result == EXIT_SUCCESS) fprintf(stdout, "SUCCESS - The test playground ended with the expected result\n");
        else fprintf(stdout, "FAILURE - The test playground ended without the expected result\n");
        return EXIT_SUCCESS;
    }


    fprintf(stdout, "\n- Telecom Nancy File System -\n\n");
    fprintf(stdout, "Checking arguments...\n\n");
    
    if (argc <= 1) {
        fprintf(stdout, "No arguments have been specified. Starting TNFS in default mode.\n\n");
    } else {
        for (int i = 1; i < argc; i++) {
            if (str_equals(argv[i], "-q") || str_equals(argv[i], "--quiet")) 
            {
                G_quiet_mode = 1;
            } 
            else if (str_equals(argv[i], "-d") || str_equals(argv[i], "--debug")) 
            {
                G_debug_mode = 1;
            } 
            else if (str_equals(argv[i], "-p") || str_equals(argv[i], "--port"))
            {
                if (argv[++i] == NULL) error_exit("the port selection option has been specified but not value was affected.");

                G_port_num = (int) strtol(argv[i], NULL, 10);

                if (G_port_num <= 1023) error_exit("the specified port is invalid and cannot be used.");
            } 
            else if (str_equals(argv[i], "-c") || str_equals(argv[i], "--client"))
            {
                G_server_mode = 0;
            } 
            else if (str_equals(argv[i], "-s") || str_equals(argv[i], "--server")) 
            {
                G_client_mode = 0;
            } 
            else if (str_equals(argv[i], "--maximum-clients")) 
            {
                if (argv[++i] == NULL) error_exit("the maximum number of clients selection option has been specified but not value was affected.");

                G_maximum_number_of_client = (int) strtol(argv[i], NULL, 10);

                if (G_maximum_number_of_client <= 0) error_exit("the specified maximum number of clients is invalid and cannot be used.");
            }
            else if (str_equals(argv[i], "--help"))
            {
                fprintf(stdout,
                "USAGE: tnfs [OPTION]...\n"
                "\n"
                "With no option specified, run in default mode.\n"
                "\n"
                "-q, --quiet                                   turn on quiet mode : no logs will be printed\n"
                "-d, --debug                                   turn on debug mode \n"
                "-p, --port [PORT_NUMBER]                      select the port number on which the tnfs server must listen on\n"
                "-c, --client                                  run tnfs as client only\n"
                "-s, --server                                  run tnfs as server only\n"
                "    --maximum-clients [MAX_CLIENT_NUMBER]     select the maximum number of clients that can connect simultaneously to the local TNFS server\n"
                "    --help                                    display this help\n"
                );
                return EXIT_SUCCESS;
            }
            else
            {
                fprintf(stdout, "Error: unknown \"%s\" option. Try \"--help\" for more informations.\n", argv[i]);
                error_exit(NULL);
            }
        }
    }

    if (!G_quiet_mode)
    fprintf(stdout,
        "Starting TFNS with the following configuration:\n"
        "   quiet_mode: %d,\n"
        "   debug_mode: %d,\n"
        "   port_num: %d,\n"
        "   client_mode: %d,\n"
        "   server_mode: %d\n"
        "}\n",
        G_quiet_mode, G_debug_mode, G_port_num, G_client_mode, G_server_mode
    );

    init_random_seed();
    if (verify_file_path(LOCAL_CONFIG_FILE_PATH) == FILE_PATH_EXIST) {
        if (!G_quiet_mode) fprintf(stdout, "\nLoading local config...\n");
        if (load_local_config() == COULDNT_LOAD_LOCAL_CONFIG) {
            fprintf(stdout, "Couldnt load local config. Exiting TNFS.\n");
            return EXIT_FAILURE;
        }
    } else {
        if (!G_quiet_mode) fprintf(stdout, "\nLocal config missing. Creating a new one...\n");
        if (create_local_config() == LOCAL_CONFIG_CREATION_FAILURE) {
            fprintf(stdout, "Couldnt create local config. Exiting TNFS.\n");
            return EXIT_FAILURE;
        }
    }

    int number_of_peers_ptr = 0;
    lock_mutex(G_DB_mutex);
    Peer** list_of_peers = select_all_peers(&number_of_peers_ptr);
    unlock_mutex(G_DB_mutex);
    G_global_routing_table = init_bucket_row(0, pow(2, CID_LENGTH * 8));
    for (int i = 0; i < number_of_peers_ptr; i++) add_peer_to_bucket_row(G_global_routing_table, list_of_peers[i]);

    if (!G_quiet_mode) {fprintf(stdout, "\nLocal peer id: "); print_hash(G_local_node_id); fprintf(stdout, "\n");}

    if (G_server_mode) {
        fprintf(stdout, "\nStarting the TNFS servers threads...\n");
        G_TCP_server_thread = create_TCP_server_thread();
        if (G_TCP_server_thread == INVALID_THREAD_IDENTIFIER) error_exit("the creation of the dedicated TCP server thread failed!");
        G_UDP_server_thread = create_UDP_server_thread();
        if (G_UDP_server_thread == INVALID_THREAD_IDENTIFIER) {
            kill_thread(G_TCP_server_thread);
            error_exit("the creation of the dedicated UDP server thread failed!");
        }
    }

    if (G_client_mode) {
        fprintf(stdout, "\nStarting the TNFS client thread...\n");
        G_client_thread = create_client_thread();
        if (G_client_thread == INVALID_THREAD_IDENTIFIER) {
            if (G_server_mode) {
                kill_thread(G_TCP_server_thread);
                kill_thread(G_UDP_server_thread);
            }
            error_exit("the creation of the dedicated client thread failed!");
        }
    }

    fprintf(stdout, "\n");
    if (G_server_mode) {
        wait_for_thread(G_TCP_server_thread);
        wait_for_thread(G_UDP_server_thread);
    }
    if (G_client_mode) wait_for_thread(G_client_thread);

    return EXIT_SUCCESS;
} 
