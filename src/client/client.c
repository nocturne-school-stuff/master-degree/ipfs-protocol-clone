#include "client.h"

/* --- IMPORTS --- */
/* Common */
#include <stdlib.h>
#include <stdio.h>
#include "globals.h"
/* Networking */
#include "network_api.h"
#include "TCP_request_api.h"
#include "transmission.h"
#include "tnfs_UDP_packet.h"
/* Multithreading */
#include "multithreading_api.h"
/* Utilitaries */
#include <time.h>
#include "timeout.h"
#include "interface.h"
#include <string.h>
#include "ustring.h"
#include <arpa/inet.h>
#include "fs_api.h"

int start_client() {
    int launched = 1;
    
    fprintf(stdout, "\nTNFS client is ready! press enter to open it!\n");
    while( getchar() != '\n' ); flush_input_buffer();
    if (system("clear") == -1) {
        fprintf(stdout, "Error while cleaning the UI. exiting TNFS.\n");
        launched = 0;
    }

    while(launched) {
        fprintf(stdout, "%s", home_message);
        int choice = read_next_int();
        
        switch(choice) {
            case JOIN_NETWORK : {
                fprintf(stdout, "%s", join_network_message);
                char** full_ip_address = read_next_ip_address();
                if (full_ip_address == INVALID_INPUT) {
                    fprintf(stdout, "You must enter a valid ip address.\n");
                    break;
                }
                
                char* ip_address = clone_str(full_ip_address[0], strnlen(full_ip_address[0], MAX_INPUT_LENGTH));
                int port_number = (int) strtol(full_ip_address[1], NULL, 10);
                free(full_ip_address[0]); free(full_ip_address[1]); free(full_ip_address);

                t_socket_config config_test;
                if (inet_pton(AF_INET, ip_address, &(config_test.sin_addr)) == 0) {
                    free(ip_address);
                    fprintf(stdout, "You must enter a valid ip address.\n");
                    break;
                }

                if (0 <= port_number && port_number <= 1024) {
                    free(ip_address);
                    fprintf(stdout, "You must enter a valid ip address.\n");
                    break;
                }

                char* src_full_ip_address = concat_full_ip_addr(G_local_ip_address, G_port_num);
                Peer* bootstrap_peer = malloc(sizeof(Peer));
                strcpy(bootstrap_peer->peer_id, "FFFF");
                strcpy(bootstrap_peer->peer_ip_address, "127.0.0.1");
                bootstrap_peer->peer_source_port = 8080;
                add_peer_to_bucket_row(G_global_routing_table, bootstrap_peer);

                t_tnfs_UDP_packet* ping_packet = create_PING_tnfs_UDP_packet(0, G_local_node_id, src_full_ip_address);
                send_tnfs_UDP_packet(ip_address, port_number, ping_packet);
                destroy_tnfs_UDP_packet(ping_packet);
                free(src_full_ip_address);

                if (get_peers_request(ip_address, port_number) == TCP_REQUEST_SUCESS) fprintf(stdout, "Successfully exchanged peers!\n");
                else fprintf(stdout, "An error occured while exchanging peers. :(\n");

                free(ip_address);
                break;
            }
            case PUBLISH_FILE : {
                fprintf(stdout, "%s", publish_file_message);
                
                int file_path_length_ptr = 0;
                char* file_path = read_next_string(&file_path_length_ptr);
                if (file_path == INVALID_INPUT) {
                    fprintf(stdout, "An error occured while reading input.\n");
                    break;
                } else if (verify_file_path(file_path) == FILE_PATH_INVALID) {
                    fprintf(stdout, "You must enter a valid file path.\n");
                    free(file_path);
                    break;
                }

                short request_result = file_publishing_request(file_path, file_path_length_ptr);
                if (request_result == TCP_REQUEST_SUCESS) {
                    fprintf(stdout, "Successfully published %s!\n", file_path);
                } else {
                    fprintf(stdout, "An error has occured during the publication of %s. :(\n", file_path);
                }

                free(file_path);
                break;
            }
            case DOWNLOAD_FILE : {
                fprintf(stdout, "%s", download_file_message);

                int file_path_length_ptr = 0;
                char* file_path = read_next_string(&file_path_length_ptr);
                if (file_path == INVALID_INPUT) {
                    fprintf(stdout, "An error occured while reading input.\n");
                    break;
                } 

                short request_result = download_file_request(file_path, file_path_length_ptr);
                if (request_result == TCP_REQUEST_SUCESS) {
                    fprintf(stdout, "Successfully downloaded %s!\n", file_path);
                } else {
                    fprintf(stdout, "An error has occured during the download of %s. :(\n", file_path);
                }
                
                free(file_path);
                break;
            }
            case EXIT : {
                fprintf(stdout, "%s", exit_message);
                launched = 0;
                break;
            }
            default :
                fprintf(stdout, "%s", input_error_message);
                break;
        }

        if (!launched) break;

        fprintf(stdout, "\npress enter to continue...\n");
        while( getchar() != '\n' ); flush_input_buffer();
        if (system("clear") == -1) {
            fprintf(stdout, "Error while cleaning the UI. exiting TNFS.\n");
            break;
        }
    }

    if (G_UDP_server_thread != INVALID_THREAD_IDENTIFIER) {kill_thread(G_UDP_server_thread); G_UDP_server_thread = INVALID_THREAD_IDENTIFIER;}
    if (G_TCP_server_thread != INVALID_THREAD_IDENTIFIER) {kill_thread(G_TCP_server_thread); G_TCP_server_thread = INVALID_THREAD_IDENTIFIER;}
    
    return EXIT_SUCCESS;
}