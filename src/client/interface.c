#include "interface.h"

/* --- IMPORTS --- */
/* Common */
#include <stdlib.h>
#include "globals.h"
/* iostream */
#include <stdio.h>
#include <termios.h>
#include <unistd.h>
/* Typing */
#include "hash32.h"
/* Utilitaries */
#include <string.h>
#include <ustring.h>

/* --- CODE --- */

const char* home_message = 
    "----------------------------\n"
    " _____  _   _  _____  ____\n"  
    "|_   _|| \\ | ||  ___|/ ___|\n" 
    "  | |  |  \\| || |_   \\___ \\ \n" 
    "  | |  | |\\  ||  _|   ___) |\n"
    "  |_|  |_| \\_||_|    |____/\n"
    "----------------------------\n"
    "\n"
    "Welcome on TNFS : a decentralized file system!\n"
    "\n"
    "What do you want to do?\n"
    "   1- Join a network.\n"
    "   2- Publish a file.\n"
    "   3- Download a file.\n"
    "   4- Exit TNFS :(\n"
    "\n";

const char* join_network_message = 
    "--- Joining a network ---"
    "Please enter the address of the network you want to join (format: \"xxx.xxx.xxx.xxx:pppp\") : \n";
const char* publish_file_message = 
    "--- Publishing a new file! ---\n"
    "Please enter the path of the file you want to publish : \n";
const char* download_file_message = 
    "--- Downloading a file from remote! ---\n"
    "Please enter the name of the file you want to download from remote : \n";
const char* exit_message = 
    "--- Exiting TNFS ---\n"
    "Execution will stop when locals TNFS servers are shutdown...\n";
const char* input_error_message =
    "Unrecognized options.\n";

void flush_input_buffer() {
    int stdin_cpy = dup(STDIN_FILENO);
    tcdrain(stdin_cpy); tcflush(stdin_cpy, TCIFLUSH);
    close(stdin_cpy);
}

char* read_next_string(int* length_ptr) {
    char buffer[MAX_INPUT_LENGTH]; bzero(buffer, MAX_INPUT_LENGTH);

    flush_input_buffer();
    fprintf(stdout, "[TNFS] ... "); print_hash(G_local_node_id); fprintf(stdout, " > "); 
    if (fgets(buffer, MAX_INPUT_LENGTH, stdin) == NULL) return INVALID_INPUT;

    *length_ptr = strnlen(buffer, MAX_INPUT_LENGTH) - 1;

    return clone_str(buffer, *length_ptr);
}

int read_next_int() {
    int length_ptr = -1;
    char* input = read_next_string(&length_ptr);
    
    if (input == INVALID_INPUT) return 0;
    
    if (9 < length_ptr) {
        free(input);
        return 0;
    }

    int input_int = (int) strtol(input, NULL, 10);
    free(input);

    return input_int;
}

hash32_char_t* read_next_hash32() {
    int length_ptr = -1;
    char* input = read_next_string(&length_ptr);

    if (input == INVALID_INPUT) return INVALID_INPUT;

    if (length_ptr != 8) {
        free(input);
        return INVALID_INPUT;
    }

    hash32_t hash = (hash32_t) strtoul(input, NULL, 16);
    free(input);
    

    return hash32_to_string(hash);
}

char** read_next_ip_address() {
    int length_ptr = -1;
    char* input = read_next_string(&length_ptr);

    if (input == INVALID_INPUT) return INVALID_INPUT;

    char** ip_address = str_split(input, length_ptr, ":");
    free(input);
    
    return (ip_address == NULL) ? INVALID_INPUT : ip_address;
}