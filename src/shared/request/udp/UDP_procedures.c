#include "UDP_procedures.h"

/* --- IMPORTS --- */
/* Networking */
#include "tnfs_UDP_packet.h"
#include "transmission.h"
#include "peersDAO.h"
/* Utilitaries */
#include "globals.h"
#include <string.h>
#include "ustring.h"
#include <stdio.h>
#include "kbucket.h"

/* --- CODE --- */
short handle_ping_procedure(t_tnfs_UDP_packet* packet) {
    return handle_ping_response_procedure(packet);
}

short handle_ping_response_procedure(t_tnfs_UDP_packet* packet) {
    char* full_id_addr = clone_str((char*) packet->payload, packet->payload_size);

    char** splitted = str_split(full_id_addr, packet->payload_size, ":"); 

    char* ip_address = splitted[0];
    int port_num = strtol(splitted[1], NULL, 10);
    
    Peer* src_peer = malloc(sizeof(Peer));
    hash32_char_t* src_peer_id = hash32_to_string(packet->src_node_id);
    strcpy(src_peer->peer_id, src_peer_id);
    strcpy(src_peer->peer_ip_address, ip_address);
    src_peer->peer_source_port = port_num;

    if (find_peer(G_global_routing_table, string_to_hash32(src_peer->peer_id)) == NULL) {
        Peer* peer_cpy = malloc(sizeof(Peer));
        strcpy(peer_cpy->peer_id, src_peer->peer_id);
        strcpy(peer_cpy->peer_ip_address, src_peer->peer_ip_address);
        peer_cpy->peer_source_port = src_peer->peer_source_port;
        add_peer_to_bucket_row(G_global_routing_table, peer_cpy);
        lock_mutex(G_DB_mutex);
        insert_peer(peer_cpy);
        unlock_mutex(G_DB_mutex);
    }

    free(src_peer); free(src_peer_id);
    free(splitted[0]); free(splitted[1]); free(splitted);
    free(full_id_addr);
    return UDP_PROCEDURE_SUCESS;
}

short handle_find_file_procedure(t_tnfs_UDP_packet* packet) {

}
short handle_find_file_reponse_procedure(t_tnfs_UDP_packet* packet) {

}
