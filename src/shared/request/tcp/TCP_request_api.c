#include "TCP_request_api.h"

/* --- IMPORTS --- */
/* Networking */
#include "tnfs_TCP_packet.h"
#include "tnfs_UDP_packet.h"
#include "transmission.h"
#include "network_api.h"
/* Procedures */
#include "TCP_procedures.h"
/* Typing */
#include "connection.h"
#include "hash32.h"
/* DAO */
#include "peersDAO.h"
#include "filesDAO.h"
/* Utilitaries */
#include <stdio.h>
#include "globals.h"
#include "ustring.h"
#include "fs_api.h"
#include <string.h>

/* --- CODE --- */

short abort_TCP_request(char* abort_reason, short abort_code) {
    fprintf(stdout, "%s\t aborting with code %d", abort_reason, abort_code);
    return abort_code;
}

t_socket_fd initiate_TCP_request(char* ip_address, int port_num, hash32_t dest_node) {
    fprintf(stdout, "Connecting to target=%s:%d...\n", ip_address, port_num);
    const t_socket_fd socket_fd = connect_to_TCP_server(ip_address, port_num);
    
    if (socket_fd == INVALID_SOCKET_FD) return INVALID_SOCKET_FD;
    fprintf(stdout, "Connected to target.\n");

    fprintf(stdout, "Synchronizing with target...\n");
    int retry_count = 0;

    do {
        t_tnfs_TCP_packet* synchronization_packet;
        const short response = read_tnfs_TCP_packet(socket_fd, &synchronization_packet);

        if (response == READING_ERROR) {
            close_socket(socket_fd);
            return INVALID_SOCKET_FD;
        }

        const t_tnfs_packet_type type = synchronization_packet->type;
        destroy_tnfs_TCP_packet(synchronization_packet);

        if (response == REQUEST_SUCESS && type == F_TNFS_SYN) break;

        if (retry_count == 3) {
            close_socket(socket_fd);
            return INVALID_SOCKET_FD;
        }
        
        t_tnfs_TCP_packet* retry_packet = create_RETRY_tnfs_TCP_packet(dest_node, G_local_node_id, 0); 
        send_tnfs_TCP_packet(socket_fd, retry_packet);
        destroy_tnfs_TCP_packet(retry_packet);
        retry_count += 1;
    } while(1);

    t_tnfs_TCP_packet* ack_packet = create_ACK_tnfs_TCP_packet(dest_node, G_local_node_id); 
    send_tnfs_TCP_packet(socket_fd, ack_packet);
    destroy_tnfs_TCP_packet(ack_packet);

    t_tnfs_TCP_packet* ack_response_packet;
    read_tnfs_TCP_packet(socket_fd, &ack_response_packet);
    destroy_tnfs_TCP_packet(ack_response_packet);

    return socket_fd;
}

hash32_t handle_initiate_TCP_request(t_socket_fd socket_fd) {
    fprintf(stdout, "[CLIENT %d] synchronizing...\n", socket_fd);
    t_tnfs_TCP_packet* synchronization_packet = create_SYN_tnfs_TCP_packet(0, G_local_node_id);
    send_tnfs_TCP_packet(socket_fd, synchronization_packet);
    destroy_tnfs_TCP_packet(synchronization_packet);

    t_tnfs_TCP_packet* ack_response_packet;
    read_tnfs_TCP_packet(socket_fd, &ack_response_packet);
    hash32_t dest_node = ack_response_packet->src_node_id;
    destroy_tnfs_TCP_packet(ack_response_packet);

    t_tnfs_TCP_packet* ack_packet = create_ACK_tnfs_TCP_packet(dest_node, G_local_node_id); 
    send_tnfs_TCP_packet(socket_fd, ack_packet);
    destroy_tnfs_TCP_packet(ack_packet);

    return dest_node;
}

void end_TCP_request(t_socket_fd socket_fd, hash32_t dest_node) {
    fprintf(stdout, "[CLIENT %d] desynchronization with target...\n", socket_fd);
    t_tnfs_TCP_packet* end_synchronization_packet = create_END_SYN_tnfs_TCP_packet(dest_node, G_local_node_id);
    send_tnfs_TCP_packet(socket_fd, end_synchronization_packet);
    destroy_tnfs_TCP_packet(end_synchronization_packet);
    fprintf(stdout, "[CLIENT %d] disconnecting...\n", socket_fd);
    close_socket(socket_fd);
}

void handle_end_TCP_request(t_socket_fd socket_fd) {
    fprintf(stdout, "[CLIENT %d] desynchronization with target...\n", socket_fd);    
    t_tnfs_TCP_packet* client_syn_end_packet;
    read_tnfs_TCP_packet(socket_fd, &client_syn_end_packet);
    destroy_tnfs_TCP_packet(client_syn_end_packet);
    fprintf(stdout, "[CLIENT %d] disconnecting...\n", socket_fd);
    close_socket(socket_fd);
}

short download_file_request(char* file_name, int file_name_length) {
    hash32_t file_cid = hash32(file_name, file_name_length);

    fprintf(stdout, "Resolving owner of "); print_hash(file_cid); fprintf(stdout, "...\n");
    hash32_char_t* file_cid_as_string = hash32_to_string(file_cid);
    lock_mutex(G_DB_mutex);
    File* file = select_by_cid(file_cid_as_string);
    unlock_mutex(G_DB_mutex);
    free(file_cid_as_string);

    Peer* destination_peer = NULL;
    if (file == NULL) destination_peer = find_nearest_peer(G_global_routing_table, file_cid);
    else {
        lock_mutex(G_DB_mutex);
        destination_peer = select_by_peer_id(file->hosting_peer_id);
        unlock_mutex(G_DB_mutex);
    }
    if (destination_peer == NULL) return TCP_REQUEST_FAILED;
    const hash32_t dest_node = string_to_hash32(destination_peer->peer_id); char* ip_addr = "127.0.0.1"; int port_num = 8080;
    free(destination_peer);

    char download_save_dir[] = "download/";
    char* save_path = malloc(sizeof(char) * (9 + file_name_length + 1));
    bzero(save_path, sizeof(char) * (9 + file_name_length + 1));
    sprintf(save_path, "%s%s", download_save_dir, file_name);

    const t_socket_fd socket_fd = initiate_TCP_request(ip_addr, port_num, dest_node);
    if (socket_fd == INVALID_SOCKET_FD) {
        return abort_TCP_request("An error occured during the connection to the target.", TCP_REQUEST_FAILED);
    } else fprintf(stdout, "Synchronized with target. Querying download procedure...\n");
    
    t_tnfs_TCP_packet* download_packet = create_DOWNLOAD_tnfs_TCP_packet(dest_node, G_local_node_id, file_cid);
    const short response = send_tnfs_TCP_packet(socket_fd, download_packet);
    destroy_tnfs_TCP_packet(download_packet);

    if (response != REQUEST_SUCESS) {
        close_socket(socket_fd);
        return abort_TCP_request("Could not start download : retry count exceeded.", TCP_REQUEST_FAILED);
    }
    
    const short procedure_result = download_file(socket_fd, G_local_node_id, dest_node, save_path);

    end_TCP_request(socket_fd, dest_node);
    return (procedure_result == TCP_PROCEDURE_SUCESS) ? TCP_REQUEST_SUCESS : TCP_REQUEST_FAILED;
}

short handle_download_file_request(t_socket_fd socket_fd, hash32_t dest_node, hash32_t src_node, hash32_t file_cid) {
    hash32_char_t* file_cid_as_string = hash32_to_string(file_cid);

    lock_mutex(G_DB_mutex);
    File* file = select_by_cid(file_cid_as_string);
    printf("%s\n", file->path);
    unlock_mutex(G_DB_mutex);
    if (file == NULL) {
        return abort_TCP_procedure("Resource not found", RESSOURCE_NOT_FOUND);
    } else {
        free(file);
    }
    
    free(file_cid_as_string);

    short result = transfer_file(socket_fd, src_node, dest_node, file_cid);
    return result;
}

short file_publishing_request(char* file_path, int file_path_length) {
    int file_name_length_ptr = 0;
    char* file_name = get_file_name_from_file_path(file_path, file_path_length, &file_name_length_ptr);

    const hash32_t file_cid = hash32(file_name, file_name_length_ptr);
    fprintf(stdout, "Resolving future recepient of "); print_hash(file_cid); fprintf(stdout, "...\n");
    
    Peer* destination_peer = find_nearest_peer(G_global_routing_table, file_cid);
    if (destination_peer == NULL) return TCP_REQUEST_FAILED;

    const hash32_t dest_node = string_to_hash32(destination_peer->peer_id); char* ip_addr = "127.0.0.1"; int port_num = 8080;
    const t_socket_fd socket_fd = initiate_TCP_request(ip_addr, port_num, dest_node);
    if (socket_fd == INVALID_SOCKET_FD) return abort_TCP_request("An error occured during the connection to the target.", TCP_REQUEST_FAILED);
    else fprintf(stdout, "Synchronized with target. Querying transfer procedure...\n");

    File* file = malloc(sizeof(File));
    hash32_char_t* file_cid_as_string = hash32_to_string(file_cid);
    strcpy(file->cid, file_cid_as_string);
    strcpy(file->name, file_name);
    hash32_char_t* dest_peer_id_as_string = hash32_to_string(dest_node);
    strcpy(file->hosting_peer_id, dest_peer_id_as_string);
    strcpy(file->path, file_path);

    lock_mutex(G_DB_mutex);
    insert_file(file);
    unlock_mutex(G_DB_mutex);
    
    free(file_cid_as_string);
    free(dest_peer_id_as_string);
    free(file);

    t_tnfs_TCP_packet* publish_packet = create_PUBLISH_tnfs_TCP_packet(dest_node, G_local_node_id, file_name, file_name_length_ptr);
    const short publish_response = send_tnfs_TCP_packet(socket_fd, publish_packet);
    destroy_tnfs_TCP_packet(publish_packet);

    if (publish_response != REQUEST_SUCESS) {
        close_socket(socket_fd);
        return abort_TCP_request("Could not start transfer : retry count exceeded.", TCP_REQUEST_FAILED);
    }

    t_tnfs_TCP_packet* synchronization_packet;
    const short synchronization_response = read_tnfs_TCP_packet(socket_fd, &synchronization_packet);
    destroy_tnfs_TCP_packet(synchronization_packet);

    if (publish_response != REQUEST_SUCESS) {
        close_socket(socket_fd);
        return abort_TCP_request("Could not start transfer : cannot synchronize with target.", TCP_REQUEST_FAILED);
    }

    const short procedure_result = transfer_file(socket_fd, G_local_node_id, dest_node, file_cid);

    return procedure_result;
}

short handle_file_publishing_request(t_socket_fd socket_fd, hash32_t dest_node, hash32_t src_node, char* file_name, int file_name_length) {
    char local_save_dir[] = "local/";

    char* save_path = malloc(sizeof(char) * (6 + file_name_length + 1));
    bzero(save_path, sizeof(char) * (6 + file_name_length + 1));
    sprintf(save_path, "%s%s", local_save_dir, file_name);

    File* file = malloc(sizeof(File));
    hash32_char_t* file_cid_as_string = hash32_to_string(hash32(file_name, file_name_length));
    strcpy(file->cid, file_cid_as_string);
    strcpy(file->name, file_name);
    hash32_char_t* dest_peer_id_as_string = hash32_to_string(dest_node);
    strcpy(file->hosting_peer_id, dest_peer_id_as_string);
    strcpy(file->path, save_path);
    lock_mutex(G_DB_mutex);
    insert_file(file);
    unlock_mutex(G_DB_mutex);

    free(file_cid_as_string);
    free(dest_peer_id_as_string);
    free(file);

    short result = download_file(socket_fd, src_node, dest_node, save_path);
    free(save_path);
    return result;
}

short get_peers_request(char* ip_addr, int port_num) {
    const t_socket_fd socket_fd = initiate_TCP_request(ip_addr, port_num, 0);
    if (socket_fd == INVALID_SOCKET_FD) return abort_TCP_request("An error occured during the connection to the target.", TCP_REQUEST_FAILED);
    else fprintf(stdout, "Synchronized with target. Querying peers sharing procedure...\n");
    
    t_tnfs_TCP_packet* get_peers_packet = create_GET_PEERS_tnfs_TCP_packet(0, G_local_node_id);
    const short response = send_tnfs_TCP_packet(socket_fd, get_peers_packet);
    destroy_tnfs_TCP_packet(get_peers_packet);

    if (response != REQUEST_SUCESS) {
        close_socket(socket_fd);
        return abort_TCP_request("Could not start peers sharing : retry count exceeded.", TCP_REQUEST_FAILED);
    }

    do {
        t_tnfs_TCP_packet* peers_packet;
        const short response = read_tnfs_TCP_packet(socket_fd, &peers_packet);
        if (peers_packet->type == F_TNFS_GET_PEERS_RESPONSE) {
            Peer* peer = (Peer*) peers_packet->payload;

            Peer* peer_test = find_peer(G_global_routing_table, string_to_hash32(peer->peer_id));
            if (peer_test == NULL) {
                Peer* peer_cpy = malloc(sizeof(Peer));
                strcpy(peer_cpy->peer_id, peer->peer_id);
                strcpy(peer_cpy->peer_ip_address, peer->peer_ip_address);
                peer_cpy->peer_source_port = peer->peer_source_port;
                lock_mutex(G_DB_mutex);
                insert_peer(peer_cpy);
                unlock_mutex(G_DB_mutex);
                add_peer_to_bucket_row(G_global_routing_table, peer_cpy);
            } else free(peer_test);
    
            const hash32_t peer_dest_node = string_to_hash32(peer->peer_id);
            char* full_ip_addr = concat_full_ip_addr(peer->peer_ip_address, peer->peer_source_port); 

            t_tnfs_UDP_packet* ping_packet = create_PING_tnfs_UDP_packet(peer_dest_node, G_local_node_id, full_ip_addr);
            send_tnfs_UDP_packet(peer->peer_ip_address, peer->peer_source_port, ping_packet);
            fprintf(stdout, "[TNFS] ... [UDP] ... "); print_hash(G_local_node_id); fprintf(stdout, " sent a TNFS_PING_PACKET to "); print_hash(ping_packet->dest_node_id); fprintf(stdout, "\n");
            destroy_tnfs_UDP_packet(ping_packet);

            free(full_ip_addr);
        } else if (peers_packet->type == F_TNFS_GET_PEERS_END) {
            destroy_tnfs_TCP_packet(peers_packet);
            break;
        } 

        destroy_tnfs_TCP_packet(peers_packet);
    } while (1);    

    fprintf(stdout, "Peer sharing ended.\n");
    end_TCP_request(socket_fd, 0);
    return TCP_REQUEST_SUCESS;
}

short handle_get_peers_request(t_socket_fd socket_fd, hash32_t dest_node) {
    return share_peers(socket_fd, G_local_node_id, dest_node);
}