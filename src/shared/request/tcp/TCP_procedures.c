#include "TCP_procedures.h"

/* --- IMPORTS --- */
/* Networking */
#include "tnfs_packet_type.h"
#include "tnfs_TCP_packet.h"
#include "transmission.h"
/* File System */
#include "fs_api.h"
/* Typing */
#include "connection.h"
#include "hash32.h"
/* DAO */
#include "peersDAO.h"
#include "filesDAO.h"
/* Utilitaries */
#include <stdio.h>
#include "globals.h"

/* --- CODE --- */

short abort_TCP_procedure(char* abort_reason, short abort_code) {
    fprintf(stdout, "%s\taborting procedure.\n", abort_reason);
    return abort_code;
}

short download_file(t_socket_fd socket_fd, hash32_t src_node, hash32_t dest_node, char* save_path) {
    const file_fd_t file_fd = create_file(save_path);

    if (file_fd == INVALID_FILE_DESCRIPTOR) return abort_TCP_procedure("Could not write file : please verify the save path of the file.", FILE_SHARING_ERROR);
    else fprintf(stdout, "[CLIENT %d] File created.\n", socket_fd);

    t_tnfs_TCP_packet* client_ready_packet = create_SYN_tnfs_TCP_packet(dest_node, src_node);
    send_tnfs_TCP_packet(socket_fd, client_ready_packet);
    destroy_tnfs_TCP_packet(client_ready_packet);

    fprintf(stdout, "[CLIENT %d] Beginning download...\n", socket_fd);
    int retry_count = 0;
    int block_count = 0; 
    do {
        t_tnfs_TCP_packet* block_packet;
        const short response = read_tnfs_TCP_packet(socket_fd, &block_packet);

        fprintf(stdout, "[CLIENT %d] HIT %d.\n", socket_fd, block_count);

        if (response == REQUEST_CORRUPTED) {
            fprintf(stdout, "[CLIENT %d] HIT %d is corrupted, retrying...\n", socket_fd, block_count);
            t_tnfs_TCP_packet* retry_packet = create_RETRY_tnfs_TCP_packet(dest_node, src_node, block_count); 
            send_tnfs_TCP_packet(socket_fd, retry_packet);
            destroy_tnfs_TCP_packet(retry_packet);
            retry_count += 1;
        } else {
            if (block_packet->type == F_TNFS_FILE_BLOCK) {
                if (write_block_in_file(file_fd, (byte*) block_packet->payload, block_packet->payload_size) != block_packet->payload_size) 
                    return abort_TCP_procedure("Could not perform download : internal file writing error.", FILE_SHARING_ERROR);
                
                t_tnfs_TCP_packet* ack_packet = create_ACK_tnfs_TCP_packet(dest_node, src_node);
                send_tnfs_TCP_packet(socket_fd, ack_packet);
                destroy_tnfs_TCP_packet(ack_packet);

                fprintf(stdout, "[CLIENT %d] HIT %d OK.\n", socket_fd, block_count);
                retry_count = 0;
                block_count += 1;
            } else if (block_packet->type == F_TNFS_DOWNLOAD_END) {
                destroy_tnfs_TCP_packet(block_packet);
                break;
            } 
        }

        if (retry_count == 3) return abort_TCP_procedure("Could not perform download : retry count exceeded.", FILE_SHARING_ERROR);

    } while (1);    
    fprintf(stdout, "[CLIENT %d] Download finished with success!\n", socket_fd);
    close_file(file_fd);

    return TCP_PROCEDURE_SUCESS;
}

short transfer_file(t_socket_fd socket_fd, hash32_t src_node, hash32_t dest_node, hash32_t file_cid) {
    hash32_char_t* file_cid_as_string = hash32_to_string(file_cid);

    lock_mutex(G_DB_mutex);
    File* file = select_by_cid(file_cid_as_string);
    unlock_mutex(G_DB_mutex);
    free(file_cid_as_string);

    if (file == NULL) return abort_TCP_procedure("Could not perform : cannot find ressource", RESSOURCE_NOT_FOUND);

    file_fd_t file_fd = open_file(file->path);
    free(file);
    
    if (file_fd == INVALID_FILE_DESCRIPTOR) return abort_TCP_procedure("Could not perform transfer : cannot find ressource.", FILE_SHARING_ERROR);
    else fprintf(stdout, "[CLIENT %d] waiting for client readiness to perform transfer...\n", socket_fd);

    fprintf(stdout, "[CLIENT %d] client ready. Performing transfer...\n", socket_fd);
    int bytes_read = 0;
    do {
        byte* block = read_next_block(file_fd, &bytes_read);
        t_tnfs_TCP_packet* block_packet = create_FILE_BLOCK_tnfs_TCP_packet(dest_node, src_node, block, bytes_read);

        int retry_count = 0;
        do {
            send_tnfs_TCP_packet(socket_fd, block_packet);
            
            t_tnfs_TCP_packet* response_packet;
            read_tnfs_TCP_packet(socket_fd, &response_packet);
            const short response_packet_type = response_packet->type;
            destroy_tnfs_TCP_packet(response_packet);

            if (response_packet_type == F_TNFS_RETRY) {
                if (++retry_count == 3) return abort_TCP_procedure("Could not perform transfer : retry count exceeded.", FILE_SHARING_ERROR);
            } else {
                retry_count = 0;
                break;
            }
        } while (1);

        destroy_tnfs_TCP_packet(block_packet);
        free(block);
    } while (bytes_read != EOF_REACHED);

    fprintf(stdout, "[CLIENT %d] Transfer done. Ending procedure.\n", socket_fd);

    t_tnfs_TCP_packet* download_end_packet = create_DOWNLOAD_END_tnfs_TCP_packet(dest_node, src_node);
    send_tnfs_TCP_packet(socket_fd, download_end_packet);
    destroy_tnfs_TCP_packet(download_end_packet);
    return TCP_PROCEDURE_SUCESS;
}

short share_peers(t_socket_fd socket_fd, hash32_t src_node, hash32_t dest_node) {
    int peers_count = 0;
    lock_mutex(G_DB_mutex);
    Peer** peers = select_all_peers(&peers_count); 
    unlock_mutex(G_DB_mutex);

    fprintf(stdout, "[CLIENT %d] Peers sharing procedure starting.\n", socket_fd);
    
    for (int i = 0; i < peers_count; i++) {
        t_tnfs_TCP_packet* peer_packet = create_TNFS_GET_PEERS_RESPONSE_TCP_packet(dest_node, src_node, peers[i]);
        send_tnfs_TCP_packet(socket_fd, peer_packet);
        destroy_tnfs_TCP_packet(peer_packet);
        free(peers[i]);
    }

    fprintf(stdout, "[CLIENT %d] Peers sharing done. Ending procedure.\n", socket_fd);

    t_tnfs_TCP_packet* peers_sharing_end_packet = create_GET_PEERS_END_tnfs_TCP_packet(dest_node, src_node);
    send_tnfs_TCP_packet(socket_fd, peers_sharing_end_packet);
    destroy_tnfs_TCP_packet(peers_sharing_end_packet);

    return TCP_PROCEDURE_SUCESS;

}