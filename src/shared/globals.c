#include "globals.h"

/* --- IMPORTS --- */
/* Common */
#include <stdlib.h>
/* Typing */
#include "hash32.h"
#include "multithreading_api.h"
#include "kbucket.h"
#include <sys/select.h>

/* --- CODE --- */
hash32_t G_local_node_id = 0;
thread_identifier G_client_thread = INVALID_THREAD_IDENTIFIER;
thread_identifier G_TCP_server_thread = INVALID_THREAD_IDENTIFIER;
thread_identifier G_UDP_server_thread = INVALID_THREAD_IDENTIFIER;
short G_number_of_connected_clients = 0;
short G_maximum_number_of_client = DEFAULT_MAXIMUM_NUMBER_OF_CLIENTS;
thread_identifier* G_server_clients_thread = NULL;
BucketRow* G_global_routing_table = NULL;
mutex_t G_DB_mutex = PTHREAD_MUTEX_INITIALIZER;

char* G_local_ip_address = "127.0.0.1";

short G_quiet_mode = 0;
short G_debug_mode = 0;
short G_port_num = DEFAULT_PORT_NUM;
short G_client_mode = 1;
short G_server_mode = 1;