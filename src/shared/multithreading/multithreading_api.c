#include "multithreading_api.h"

/* --- IMPORTS --- */
/* Common */
#include <stdlib.h>
#include "globals.h"
/* Multithreading */
#include <pthread.h>
/* Client & Servers */
#include "client.h"
#include "TCPServer.h"
#include "UDPServer.h"
/* Utilitaries */
#include "uerror.h"

thread_identifier create_new_thread(const void* function_ptr, const void* args) {
    thread_identifier t_id;
    if (pthread_create(&t_id, NULL, function_ptr, (void*) args)) return INVALID_THREAD_IDENTIFIER;
    return t_id;
}

thread_identifier create_TCP_server_thread() {
    return create_new_thread(start_TCP_server, NULL);
}

thread_identifier create_UDP_server_thread() {
    return create_new_thread(start_UDP_server, NULL);
}

thread_identifier create_client_thread() {
    return create_new_thread(start_client, NULL);
}

short wait_for_thread(thread_identifier t_id) {
    if (t_id == INVALID_THREAD_IDENTIFIER) return CANNOT_FIND_THREAD;
    return pthread_join(t_id, NULL) ? CANNOT_FIND_THREAD : THREAD_FINISHED;
}

short kill_thread(thread_identifier t_id) {
    return pthread_cancel(t_id) ? CANNOT_CANCEL_THREAD : THREAD_CANCELLED;
}

mutex_t create_mutex() {
    mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
    return mutex;
}

short lock_mutex(mutex_t mutex) {
    return pthread_mutex_lock(&mutex) ? MUTEX_LOCKING_ERROR : MUTEX_LOCKED;
}

short unlock_mutex(mutex_t mutex) {
    return pthread_mutex_unlock(&mutex) ? MUTEX_UNLOCKING_ERROR : MUTEX_UNLOCKED;
}