#include "random.h"

/* --- IMPORTS --- */
/* Common */
#include <stdlib.h>
/* Utilitaries */
#include <time.h>

/* --- Code --- */
static time_t random_seed = 0;
const char* alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.+-*/ %$£€&'\"(_)=~#{[|`\\^@]}";
const short alphabet_length = 90;

void init_random_seed() {
    random_seed = time(NULL);
    srand(random_seed);
}

int random_int(int min, int max) {
    return (rand() % (max + 1)) + min;  
}

char random_char() {
    return alphabet[random_int(0, alphabet_length - 1)];
}

char* random_string(int* length_ptr, const int min_length, const int max_length) {
    *length_ptr = random_int(min_length, max_length);
    char* string = (char*) malloc(sizeof(char) * (*length_ptr + 1));

    for (int i = 0; i < *length_ptr; i++) string[i] = random_char();
    string[*length_ptr] = '\0';

    return string;
}