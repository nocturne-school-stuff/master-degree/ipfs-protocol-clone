#include "ustring.h"

/* --- IMPORTS --- */
/* Common */
#include <stdlib.h>
#include <stdio.h>
/* Utilitaries */
#include <string.h>
#include <math.h>

/* --- CODE --- */
char* clone_str(const char* to_clone, const int to_clone_length) {
    char* cloned = (char*) malloc(sizeof(char) * (to_clone_length + 1));
    strncpy(cloned, to_clone, to_clone_length);
    cloned[to_clone_length] = '\0';
    return cloned;
}


short str_equals(const char* str_1, const char* str_2) {
    return (strcmp(str_1, str_2) == 0) ? STRINGS_EQUALS : STRINGS_NOT_EQUALS; 
}

char* integer_to_string(const int i, int* length_ptr) {
    *length_ptr = ((int) log10(i)) + 1;
    char* buffer = (char*) malloc(sizeof(char) * (*length_ptr + 1));
    sprintf(buffer, "%d", i);
    buffer[*length_ptr] = '\0';
    return buffer;
}

char* str_concat(const char* str_1, const char* str_2) {
    const int total_length = strnlen(str_1, 16) + strnlen(str_2, 6);
    char* buffer = malloc(sizeof(char) * (total_length + 1));
    strcat(buffer, str_1);
    strcat(buffer, str_2);
    buffer[total_length] = '\0';
    return buffer;
}

char** str_split(const char* str, int total_length, const char* delimeter) {
    int sub_len = strcspn(str, delimeter);
    if (sub_len == total_length) return NULL;

    char** splited = malloc(sizeof(char*) * 2);
    
    char* first = malloc(sizeof(char) * (sub_len + 1));
    strncpy(first, str, sub_len); first[sub_len] = '\0';
    
    int second_length = total_length - sub_len - 1;
    char* second = malloc(sizeof(char) * (second_length + 1));
    strncpy(second, str + sub_len + 1, second_length); second[second_length] = '\0';

    splited[0] = first;
    splited[1] = second;

    return splited;
}

char* concat_full_ip_addr(char* ip_addr, int port_num) {
    int port_len_ptr = 0;
    char* port_as_string = integer_to_string(port_num, &port_len_ptr);
    char* result = malloc(sizeof(char) * (strnlen(ip_addr, 16) + 1 + port_len_ptr + 1));
    memcpy(result, ip_addr, strnlen(ip_addr, 16));
    memcpy(result + strnlen(ip_addr, 16), ":", 1);
    memcpy(result + strnlen(ip_addr, 16) + 1, port_as_string, port_len_ptr);
    result[strnlen(ip_addr, 16) + 1 + port_len_ptr] = '\0';
    return result;
}