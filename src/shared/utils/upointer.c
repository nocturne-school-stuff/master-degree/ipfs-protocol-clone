#include "upointer.h"

/* --- IMPORTS --- */
/* Common */
#include <stdlib.h>

/* --- CODE --- */

int* int_pointer(int value) {
    int* ptr = (int*) malloc(sizeof(int));
    *ptr = value;
    return ptr;
}
