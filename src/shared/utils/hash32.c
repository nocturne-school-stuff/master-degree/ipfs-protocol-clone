#include "hash32.h"

/* --- IMPORTS --- */
/* Common */
#include <stdio.h>
/* Hashing */
#include <openssl/sha.h>
/* Random */
#include "random.h"
/* Utilitaries */
#include "ustring.h"

/* --- CODE --- */

hash32_t hash32(char* to_hash, const int to_hash_length) {
    unsigned char outputBuffer[SHA256_DIGEST_LENGTH];
    SHA256(to_hash, to_hash_length, outputBuffer);

    hash32_char_t* hash_as_string = (hash32_char_t*) clone_str(outputBuffer, 4);
    hash32_t hash = string_to_hash32(hash_as_string);
    free(hash_as_string);
    
    return hash;
}

hash32_t random_hash32() {
    int rstring_length;
    char* rstring = random_string(&rstring_length, 8, 32);
    hash32_t hashed_rstring = hash32(rstring, rstring_length);
    free(rstring);
    return hashed_rstring;
}

hash32_char_t* hash32_to_string(hash32_t value) {
    hash32_char_t* buffer = (hash32_char_t*) malloc(sizeof(hash32_char_t) * 5);
    
    buffer[0] = value >> 24 & 0xFF;
    buffer[1] = value >> 16 & 0xFF;
    buffer[2] = value >> 8 & 0xFF;
    buffer[3] = value;
    buffer[4] = '\0';

    return buffer;
}

hash32_t string_to_hash32(hash32_char_t value_as_string[5]) {
    hash32_t value = 0;

    value += value_as_string[0] << 24;
    value += value_as_string[1] << 16;
    value += value_as_string[2] << 8;
    value += value_as_string[3];

    return value;
}

void print_hash(hash32_t hash) {
    hash32_char_t* hash_as_string = hash32_to_string(hash);
    print_hash_string(hash_as_string);
    free(hash_as_string);
}

void print_hash_string(hash32_char_t hash_as_string[5]) {
    for(int i = 0; i < 4; i++)
        fprintf(stdout, "%02x", hash_as_string[i]);
}

unsigned long xor_metric(hash32_t hash_1, hash32_t hash_2) {
    return (unsigned long) hash_1 ^ hash_2;
}