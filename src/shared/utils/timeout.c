#include "timeout.h"

/* --- IMPORTS --- */
/* Common */
#include <stdlib.h>
/* Time */
#include <time.h>

/* --- CODE --- */
int is_timeout(const time_t start_time, time_t delay_in_ms) {
    return (time(NULL) - start_time < (delay_in_ms / 1000)) ? NO_TIMEOUT : TIMEOUT;
}
