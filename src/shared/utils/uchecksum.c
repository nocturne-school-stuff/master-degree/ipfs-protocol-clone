#include "uchecksum.h"

/* --- IMPORTS --- */
/* Common */
#include "stdlib.h"
/* Serialization */
#include "serialization.h"
/* Hashing */
#include "hash32.h"
/* Utilitaries */
#include "ustring.h"

/* --- CODE --- */
hash32_t compute_checksum(byte* data, int data_length) {
    return hash32((char*) data, data_length);
}

short verify_checksum(byte* data, int data_length, hash32_t checksum) {
    return (compute_checksum(data, data_length) == checksum) ? CHECKSUM_VALID : CHECKSUM_NOT_VALID;
}