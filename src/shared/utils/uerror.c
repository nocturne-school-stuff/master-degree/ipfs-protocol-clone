#include "uerror.h"

/* --- IMPORTS --- */
/* Common */
#include <stdlib.h>
#include <stdio.h>

/* --- CODE --- */

void error_exit(char* message) {
    if (message != NULL) fprintf(stderr, "Error: %s\n", message);

    fprintf(stdout, "terminating programm with code %d\n", EXIT_FAILURE);
    exit(EXIT_FAILURE);
}

void error_message(char* message) {
    fprintf(stderr, "Error: %s\n", message);
}