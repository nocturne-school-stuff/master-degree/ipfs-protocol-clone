#include "db_functions.h"
#include <sqlite3.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* err_msg = 0;

int read_sql_script()
{
    FILE* fPtr;
    char buffer[BUFFER_SIZE];
    int totalRead = 0;

    fPtr = fopen("script.sql", "r");

    if(fPtr == NULL)
    {
        printf("Unable to open file.\n");
        printf("Please check whether file exists and you have read privilege.\n");
        exit(EXIT_FAILURE);
    }

    while(fgets(buffer, BUFFER_SIZE, fPtr) != NULL) 
    {
        totalRead = strlen(buffer);

        buffer[totalRead - 1] = buffer[totalRead - 1] == '\n' ? '\0' : buffer[totalRead - 1];

        strcat(sql_tables, buffer);

    }

    fclose(fPtr);
    return 0;
}

int open_db(){

    int result = sqlite3_open("local_database.db", &db);
    sqlite3_busy_timeout(db, 2000);
    
    if (result != SQLITE_OK) {
        
        fprintf(stderr, "Cannot open database: %s\n", sqlite3_errmsg(db));
        sqlite3_close(db);
        return 1;
    }

    read_sql_script();

    result = sqlite3_exec(db, sql_tables, 0, 0, &err_msg);
    
    if (result != SQLITE_OK ) {
        
        fprintf(stderr, "SQL error1: %s\n", err_msg);
        sqlite3_close(db);
        
        return 1;
    } 

    return 0;
}

int callback(void *NotUsed, int argc, char **argv, char **azColName) {
    
    NotUsed = 0;
    
    for (int i = 0; i < argc; i++) {

        printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
    }
    
    printf("\n");
    
    return 0;
}

