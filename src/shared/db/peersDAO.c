#include <sqlite3.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "peersDAO.h"
#include "db_functions.h"


Peer** select_all_peers(int* nb_row){
    
    sqlite3_stmt *res;
    open_db();

    char *sql = "SELECT * FROM Peers";
    int result = sqlite3_prepare_v2(db, sql, -1, &res, 0);

    Peer** result_peer = malloc(sizeof(Peer*));
    int row_count = 0;
    if (result == SQLITE_OK ) {

        while ((result = sqlite3_step(res)) == SQLITE_ROW) {

        row_count += 1;

        Peer* result_row = malloc(sizeof(Peer));
        strcpy(result_row->peer_id, sqlite3_column_text(res, 0));
        strcpy(result_row->peer_ip_address, sqlite3_column_text(res, 1));
        result_row->peer_source_port = sqlite3_column_int(res, 2);

        result_peer = realloc(result_peer, sizeof(Peer*) * row_count);
        result_peer[row_count-1] = result_row;
        }
    } else{
        fprintf(stderr, "Failed to select data\n");
        fprintf(stderr, "SQL error2: %s\n", err_msg);

        sqlite3_close(db);
        
        free(result_peer);

        return NULL;
    }

    sqlite3_finalize(res);
    sqlite3_close(db);

    *nb_row = row_count;
    return result_peer;
}


Peer* select_by_peer_id(char peer_id[5]){

    sqlite3_stmt *res;
    open_db();
    
    char *sql = "SELECT * FROM Peers WHERE peer_id = ?";
    int result = sqlite3_prepare_v2(db, sql, -1, &res, 0);
    
    if (result == SQLITE_OK) {
        
        sqlite3_bind_text(res, 1, peer_id, 5, NULL);
    } else {
        
        fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
    }
    
    Peer* result_row = NULL;

    if ((result = sqlite3_step(res)) == SQLITE_ROW) {

        result_row = malloc(sizeof(Peer));

        strcpy(result_row->peer_id, sqlite3_column_text(res, 0));
        strcpy(result_row->peer_ip_address, sqlite3_column_text(res, 1));
        result_row->peer_source_port = sqlite3_column_int(res, 2);

    }

    sqlite3_finalize(res);
    sqlite3_close(db);

    return result_row;
}


int insert_peer(struct Peer *peer){

    sqlite3_stmt *res;
    open_db();
 
    char *sql = "INSERT INTO Peers VALUES(?, ?, ?);"; 

    int result = sqlite3_prepare_v2(db, sql, -1, &res, 0);
    
    if (result == SQLITE_OK) {
        
        sqlite3_bind_text(res, 1, peer->peer_id, 5, NULL);
        sqlite3_bind_text(res, 2, peer->peer_ip_address, 16, NULL);
        sqlite3_bind_int(res, 3, peer->peer_source_port);
    } else {
        
        fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
    }

    sqlite3_step(res); 

    sqlite3_finalize(res);
    sqlite3_close(db);
    return 0;
}


int update_peer(char peer_id[5], struct Peer *peer){

    sqlite3_stmt *res;
    open_db();
    
    char *sql = "UPDATE Peers SET peer_id = ?, peer_ip_address = ?, peer_source_port = ?";
        
    int result = sqlite3_prepare_v2(db, sql, -1, &res, 0);
    
    if (result == SQLITE_OK) {
        sqlite3_bind_text(res, 1, peer->peer_id, 5, NULL);
        sqlite3_bind_text(res, 2, peer->peer_ip_address, 16, NULL);
        sqlite3_bind_int(res, 4, peer->peer_source_port);
    } else {
        
        fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
    }

    sqlite3_step(res); 

    sqlite3_finalize(res);
    sqlite3_close(db);
    return 0;
}

int delete_peer(char peer_id[5]){

    sqlite3_stmt *res;
    open_db();
    
    char *sql = "DELETE FROM Peers WHERE peer_id = ?";
        
    int result = sqlite3_prepare_v2(db, sql, -1, &res, 0);
    
    if (result == SQLITE_OK) {
        
        sqlite3_bind_text(res, 1, peer_id, 5, NULL);
    } else {
        
        fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
    }

    sqlite3_step(res); 

    sqlite3_finalize(res);
    sqlite3_close(db);
    return 0;
}