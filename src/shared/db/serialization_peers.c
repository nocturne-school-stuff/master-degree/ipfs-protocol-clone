#include "serialization_peers.h"
#include "peersDAO.h"
#include "serialization.h"
#include "deserialization.h"
#include "hash32.h"
#include <string.h>

byte* serialize_peer(const Peer* peer) {
    int* array_of_lengths = malloc(sizeof(int) * (NUMBER_OF_FIELDS_OF_PEER + 1));
    array_of_lengths[0] = sizeof(hash32_t);
    array_of_lengths[1] = sizeof(int);
    array_of_lengths[2] = strnlen(peer->peer_ip_address, 16);
    array_of_lengths[3] = sizeof(int);

    byte** array_of_byte_array = (byte**) malloc(sizeof(byte*) * (NUMBER_OF_FIELDS_OF_PEER + 1));
    array_of_byte_array[0] = char_array_to_byte_array(peer->peer_id, sizeof(hash32_t));
    array_of_byte_array[1] = integer_to_byte_array(array_of_lengths[2]);
    array_of_byte_array[2] = char_array_to_byte_array(peer->peer_ip_address, array_of_lengths[2]);
    array_of_byte_array[3] = integer_to_byte_array(peer->peer_source_port);

    const int total_number_of_bytes = total_number_of_byte_of_peer(peer) + sizeof(int);
    int moved_bytes = 0;

    byte* peer_bytes = (byte*) malloc(sizeof(byte) * total_number_of_bytes);
    for (int i = 0; i < NUMBER_OF_FIELDS_OF_PEER + 1; i++) {
        concat_byte_arrays(peer_bytes, array_of_byte_array[i], array_of_lengths[i], &moved_bytes);
        free(array_of_byte_array[i]);
    }

    free(array_of_byte_array);
    free(array_of_lengths);

    return peer_bytes;
}

int total_number_of_byte_of_peer(const Peer* peer) {
    return sizeof(hash32_t) + strnlen(peer->peer_ip_address, 255) + sizeof(int);
}

Peer* deserialize_peer(const byte* peer_as_byte) {
    byte* ptr = peer_as_byte;
    
    char* peer_id = byte_array_to_char_array(ptr, sizeof(hash32_t));
    ptr += sizeof(hash32_t);

    int ip_addr_length = byte_array_to_integer(ptr);
    ptr += sizeof(int);
    char* ip_addr = byte_array_to_char_array(ptr, ip_addr_length);
    ptr += ip_addr_length;

    int source_port_number = byte_array_to_integer(ptr);
    ptr += sizeof(int);

    Peer* peer = malloc(sizeof(Peer));
    bzero(peer, sizeof(Peer));
    
    memcpy(peer->peer_id, peer_id, sizeof(hash32_t));
    memcpy(peer->peer_ip_address, ip_addr, ip_addr_length);
    peer->peer_source_port = source_port_number;
    
    free(peer_id);
    free(ip_addr);

    return peer;    
}