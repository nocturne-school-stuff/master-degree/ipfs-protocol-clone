#include "serialization_files.h"

/* --- IMPORTS --- */
/* Common */
#include <stdlib.h>
/* Serialization */
#include "serialization.h"
#include "deserialization.h"
/* Typing */
#include "filesDAO.h"
/* Utilitaries */
#include "hash32.h"
#include <string.h>
#include "ustring.h"

byte* serialize_file(const File* files) {
    int* array_of_lengths = (int*) malloc(sizeof(int) * (NUMBER_OF_FIELDS_OF_FILES + 2));
    array_of_lengths[0] = sizeof(char) * sizeof(hash32_t);
    array_of_lengths[1] = sizeof(char) * sizeof(hash32_t);
    array_of_lengths[2] = sizeof(int);
    array_of_lengths[3] = sizeof(char) * strnlen(files->name, 64);
    array_of_lengths[4] = sizeof(int);
    array_of_lengths[5] = sizeof(char) * strnlen(files->path, 255);

    byte** array_of_byte_array = (byte**) malloc(sizeof(byte*) * (NUMBER_OF_FIELDS_OF_FILES + 2));

    array_of_byte_array[0] = char_array_to_byte_array(files->cid, sizeof(hash32_t));
    array_of_byte_array[1] = char_array_to_byte_array(files->hosting_peer_id, sizeof(hash32_t));
    array_of_byte_array[2] = integer_to_byte_array(array_of_lengths[3]);
    array_of_byte_array[3] = char_array_to_byte_array(files->name, array_of_lengths[3]);
    array_of_byte_array[4] = integer_to_byte_array(array_of_lengths[4]);
    array_of_byte_array[5] = char_array_to_byte_array(files->path, array_of_lengths[5]);

    const int total_number_of_bytes = total_number_of_byte_of_file(files) + sizeof(int) + sizeof(int);
    int moved_bytes = 0;
    byte* file_bytes = (byte*) malloc(sizeof(byte) * total_number_of_bytes);
    for (int i = 0; i < NUMBER_OF_FIELDS_OF_FILES + 2; i++) {
        concat_byte_arrays(file_bytes, array_of_byte_array[i], array_of_lengths[i], &moved_bytes);
        free(array_of_byte_array[i]);
    }

    free(array_of_byte_array);
    free(array_of_lengths);

    return file_bytes;
}

int total_number_of_byte_of_file(const File* file) {
    return sizeof(hash32_t) + sizeof(hash32_t) + strnlen(file->name, 64) + strnlen(file->path, 255);
}

File* deserialize_file(byte* file_as_byte) {
    byte* ptr = file_as_byte;
    
    char* cid = byte_array_to_char_array(ptr, sizeof(hash32_t));
    ptr += sizeof(hash32_t);
    char* hosting_peer_id = byte_array_to_char_array(ptr, sizeof(hash32_t));
    ptr += sizeof(hash32_t);
    
    const int name_length = byte_array_to_integer(ptr);
    ptr += sizeof(int);
    char* name = byte_array_to_char_array(ptr, name_length);
    ptr += name_length;

    const int path_length = byte_array_to_integer(ptr);
    ptr += sizeof(int);
    char* path = char_array_to_byte_array(ptr, path_length);
    
    File* file = malloc(sizeof(File));
    bzero(file, sizeof(File));
    
    memcpy(file->cid, cid, sizeof(hash32_t));
    memcpy(file->hosting_peer_id, hosting_peer_id, sizeof(hash32_t));
    memcpy(file->name, name, name_length); file->name[name_length] = '\0';
    memcpy(file->path, path, path_length); file->path[path_length] = '\0';
    
    free(cid);
    free(hosting_peer_id);
    free(name);
    free(path);

    return file;    
}