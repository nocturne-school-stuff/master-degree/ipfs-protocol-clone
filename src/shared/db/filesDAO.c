#include <sqlite3.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "filesDAO.h"
#include "db_functions.h"



File** select_all_files(int* nb_row){
    
    sqlite3_stmt *res;
    open_db();

    char *sql = "SELECT * FROM Files";
    int result = sqlite3_prepare_v2(db, sql, -1, &res, 0);

    File** result_file = malloc(sizeof(File*));
    int row_count = 0;
    if (result == SQLITE_OK ) {

        while ((result = sqlite3_step(res)) == SQLITE_ROW) {

        row_count += 1;

        File* result_row = malloc(sizeof(File));
        strcpy(result_row->cid, sqlite3_column_text(res, 0));
        strcpy(result_row->name, sqlite3_column_text(res, 1));
        strcpy(result_row->path, sqlite3_column_text(res, 2));
        strcpy(result_row->hosting_peer_id, sqlite3_column_text(res, 3));

        result_file = realloc(result_file, sizeof(File*) * row_count);
        result_file[row_count-1] = result_row;
        }
    } else{
        fprintf(stderr, "Failed to select data\n");
        fprintf(stderr, "SQL error2: %s\n", err_msg);

        sqlite3_close(db);
        
        free(result_file);

        return NULL;
    }

    sqlite3_finalize(res);
    sqlite3_close(db);

    *nb_row = row_count;
    return result_file;
}


File* select_by_cid(char cid[5]){

    sqlite3_stmt *res;
    open_db();
    
    char *sql = "SELECT * FROM Files WHERE cid = ?";
    int result = sqlite3_prepare_v2(db, sql, -1, &res, 0);
    
    if (result == SQLITE_OK) {
        
        sqlite3_bind_text(res, 1, cid, 5, NULL);
    } else {
        
        fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
    }
    
    File* result_row = NULL;

    if ((result = sqlite3_step(res)) == SQLITE_ROW) {

        result_row = malloc(sizeof(File));

        strcpy(result_row->cid, sqlite3_column_text(res, 0));
        strcpy(result_row->name, sqlite3_column_text(res, 1));
        strcpy(result_row->path, sqlite3_column_text(res, 2));
        strcpy(result_row->hosting_peer_id, sqlite3_column_text(res, 3));
    }

    sqlite3_finalize(res);
    sqlite3_close(db);

    return result_row;
}


int insert_file(struct File *file){
    sqlite3_stmt *res;
    open_db();
 
    char *sql = "INSERT INTO Files VALUES(?, ?, ?, ?);"; 

    int result = sqlite3_prepare_v2(db, sql, -1, &res, 0);

    if (result == SQLITE_OK) {
        sqlite3_bind_text(res, 1, file->cid, 5, NULL);
        sqlite3_bind_text(res, 2, file->name, 64, NULL);
        sqlite3_bind_text(res, 3, file->path, 255, NULL);
        sqlite3_bind_text(res, 4, file->hosting_peer_id, 5, NULL);
    } else {
        
        fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
    }
    
    sqlite3_step(res); 
    
    sqlite3_finalize(res);
    sqlite3_close(db);
    return 0;
}



int update_file(char cid[5] , struct File *file){

    sqlite3_stmt *res;
    open_db();
    
    char *sql = "UPDATE Files SET cid = ?, name = ?, path = ?, byte_size = ?, hosting_peer_id = ? WHERE cid = ?";
        
    int result = sqlite3_prepare_v2(db, sql, -1, &res, 0);
    
    if (result == SQLITE_OK) {
        sqlite3_bind_text(res, 1, file->cid, 5, NULL);
        sqlite3_bind_text(res, 2, file->name, 64, NULL);
        sqlite3_bind_text(res, 3, file->path, 255, NULL);
        sqlite3_bind_text(res, 5, file->hosting_peer_id, 5, NULL);
        sqlite3_bind_text(res, 6, cid, 5, NULL);
    } else {
        
        fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
    }

    sqlite3_step(res); 

    sqlite3_finalize(res);
    sqlite3_close(db);
    return 0;
}


int delete_file(char cid[5]){
    sqlite3_stmt *res;
    open_db();
    
    char *sql = "DELETE FROM Files WHERE cid = ?";
        
    int result = sqlite3_prepare_v2(db, sql, -1, &res, 0);
    
    if (result == SQLITE_OK) {
        
        sqlite3_bind_text(res, 1, cid, 5, NULL);
    } else {
        
        fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
    }

    sqlite3_step(res); 

    sqlite3_finalize(res);
    sqlite3_close(db);
    return 0;
}

