#include "network_api.h"

/* --- IMPORTS --- */
/* Common */
#include <stdlib.h>
#include "globals.h"
/* Connection */
#include "connection.h"
/* Utilitaries */
#include "uerror.h"

/* --- CODE --- */
t_socket_fd host_TCP_server(const short port_num) {
    t_socket_config* config = get_server_config_on(port_num);
    const t_socket_fd socket_fd = create_server_socket(config, TCP);
    free(config);
    return socket_fd;
}

t_socket_fd connect_to_TCP_server(const char* ip_addr, const short port_num) {
    t_socket_config* config = get_socket_config_for(ip_addr, port_num);
    t_socket_fd socket_fd = create_client_socket(config, TCP);
    free(config);
    return socket_fd;
}

t_socket_fd host_UDP_server(const short port_num) {
    t_socket_config* config = get_server_config_on(port_num);
    const t_socket_fd socket_fd = create_server_socket(config, UDP);
    free(config);
    return socket_fd;
}