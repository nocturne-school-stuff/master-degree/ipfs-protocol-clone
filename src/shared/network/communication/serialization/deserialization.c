#include "deserialization.h"

/* --- IMPORTS --- */
/* Common */
#include <stdlib.h>
/* Utilitaries */
#include <string.h>

char byte_to_char(const byte b) {
    return (signed char) b;
}

char* byte_array_to_char_array(const byte* b, const int b_length) {
    char* s = (char*) malloc(sizeof(char) * (b_length + 1));

    int idx = 0;
    while(b[idx] != '\0' && idx < b_length) {
        s[idx] = byte_to_char(b[idx]);
        idx++;
    }
    s[b_length] = '\0';

    return s;
}

int byte_array_to_integer(const byte* b) {
    const int sizeof_int = sizeof(int);
    int i = 0;

    for (int j = 0; j < sizeof_int; j++) 
        i += (b[j] << (8 * (sizeof_int - j - 1)));

    return i;
}