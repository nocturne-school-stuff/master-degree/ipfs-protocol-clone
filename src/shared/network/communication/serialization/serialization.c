#include "serialization.h"

/* --- IMPORTS --- */
/* Common */
#include <stdlib.h>
/* Utilitaries */
#include <string.h>

/* --- CODE --- */
byte char_to_byte(const char c) {
    return (byte) c;
}

byte* char_array_to_byte_array(const char* s, const int s_lenght) {
    byte* bytes = (byte*) malloc(sizeof(byte) * (s_lenght + 1));

    int idx = 0;
    do {
        bytes[idx] = char_to_byte(s[idx]);
    } while (s[idx++] != '\0');

    return bytes;
}

byte* integer_to_byte_array(const int i) { 
    const int sizeof_int = sizeof(int);
    byte* bytes = (byte*) malloc(sizeof(byte) * sizeof_int);
    
    for (int j = 0; j < sizeof_int - 1; j++)
        bytes[j] = (byte) (i >> (8 * (sizeof_int - j - 1))) & 0xFF;
    bytes[sizeof_int - 1] = (byte) i & 0xFF;

    return bytes;
}

byte* concat_byte_arrays(byte* base, byte* to_concat, const int to_concat_length, int* bytes_moved) {
    memcpy(base + (*bytes_moved), to_concat, to_concat_length);
    (*bytes_moved) += to_concat_length;
}

byte* copy_byte_array(const byte* original, const int original_length) {
    byte* copy = malloc(sizeof(byte) * original_length);
    memcpy(copy, original, original_length);
    return copy;
}