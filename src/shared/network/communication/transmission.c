#include "transmission.h"

/* --- IMPORTS --- */
/* Networking */
#include <unistd.h>
#include <sys/select.h>
#include "connection.h"
#include "network_api.h"
#include "tnfs_TCP_packet.h"
#include "tnfs_UDP_packet.h"
/* Timeout handling */
#include <time.h>
#include "timeout.h"
/* Utilitaries */
#include "fs_api.h"
#include "uchecksum.h"
#include <string.h>
#include <stdio.h>
#include "deserialization.h"

/* --- CODE --- */
short send_tnfs_TCP_packet(t_socket_fd TCP_socket_fd, t_tnfs_TCP_packet* tnfs_TCP_packet) {
    const int sizeof_tnfs_TCP_packet = total_number_of_byte_of_tnfs_TCP_packet(tnfs_TCP_packet);
    byte* serialized_tnfs_TCP_packet = serialize_tnfs_TCP_packet(tnfs_TCP_packet);
    if (write(TCP_socket_fd, serialized_tnfs_TCP_packet, sizeof_tnfs_TCP_packet) != sizeof_tnfs_TCP_packet) return SENDING_ERROR;
    free(serialized_tnfs_TCP_packet);
    return REQUEST_SUCESS;
}

short read_tnfs_TCP_packet(t_socket_fd TCP_socket_fd, t_tnfs_TCP_packet** ptr_tnfs_TCP_packet) {
    fd_set read_fds, read_ready_fds;
    FD_ZERO(&read_fds);
    FD_SET(TCP_socket_fd, &read_fds);
    const time_t start_time = time(NULL);
    while(!is_timeout(start_time, REQUEST_TIMEOUT_DELAY)) {
        read_ready_fds = read_fds;
        if (0 < select(FD_SETSIZE, &read_ready_fds, NULL, NULL, NULL)) {
            const int buffer_length = BLOCK_DIGEST_LENGTH + sizeof(int) + TNFS_TCP_PACKET_HEADER_LENGTH + TNFS_TCP_PACKET_FOOTER_LENGTH;
            byte* bytes_buffer = malloc(sizeof(byte) * buffer_length); bzero(bytes_buffer, buffer_length);

            int bytes_written = 0;

            bytes_written = read(TCP_socket_fd, bytes_buffer, TNFS_TCP_PACKET_HEADER_LENGTH);

            if (bytes_written == -1) {
                free(bytes_buffer);
                return READING_ERROR;
            }

            bytes_written = read(TCP_socket_fd, bytes_buffer + TNFS_TCP_PACKET_HEADER_LENGTH, TNFS_TCP_PACKET_FOOTER_LENGTH);

            if (bytes_written == -1) {
                free(bytes_buffer);
                return READING_ERROR;
            }

            bytes_written = read(TCP_socket_fd, bytes_buffer + TNFS_TCP_PACKET_HEADER_LENGTH + TNFS_TCP_PACKET_FOOTER_LENGTH, sizeof(int));

            if (bytes_written == -1) {
                free(bytes_buffer);
                return READING_ERROR;
            }

            int total_size = TNFS_TCP_PACKET_HEADER_LENGTH + TNFS_TCP_PACKET_FOOTER_LENGTH + sizeof(int) + byte_array_to_integer(bytes_buffer + TNFS_TCP_PACKET_HEADER_LENGTH + TNFS_TCP_PACKET_FOOTER_LENGTH);
            int total_read = TNFS_TCP_PACKET_HEADER_LENGTH + TNFS_TCP_PACKET_FOOTER_LENGTH + sizeof(int);
            int rest = total_size - total_read;

            do {
                bytes_written = read(TCP_socket_fd, bytes_buffer + total_read, rest);

                if (bytes_written == -1) {
                    free(bytes_buffer);
                    return READING_ERROR;
                }

                total_read += bytes_written;
                rest -= bytes_written;
            } while (0 < rest);

            *ptr_tnfs_TCP_packet = deserialize_tnfs_TCP_packet(bytes_buffer);
            free(bytes_buffer);

            byte* serialized_payload = serialize_tnfs_packet_payload((*ptr_tnfs_TCP_packet)->payload, (*ptr_tnfs_TCP_packet)->payload_size, (*ptr_tnfs_TCP_packet)->type);
            if (verify_checksum(serialized_payload, (*ptr_tnfs_TCP_packet)->payload_size, (*ptr_tnfs_TCP_packet)->checksum) == CHECKSUM_NOT_VALID) {
                free(serialized_payload);
                return REQUEST_CORRUPTED;
            }

            free(serialized_payload);
            
            return REQUEST_SUCESS;
        }
    }

    return REQUEST_TIMEOUT;
}

short send_tnfs_UDP_packet(char* ip_address, int port_num, t_tnfs_UDP_packet* tnfs_UDP_packet) {
    t_socket_fd UDP_socket_fd = create_raw_tcp_socket(UDP);
    t_socket_config* socket_config = get_socket_config_for(ip_address, port_num); socklen_t len = sizeof(*socket_config);
    const int sizeof_packet = total_number_of_byte_of_tnfs_UDP_packet(tnfs_UDP_packet);
    byte* serialized_packet = serialize_tnfs_UDP_packet(tnfs_UDP_packet);
    sendto(UDP_socket_fd, serialized_packet, sizeof_packet, MSG_CONFIRM, (struct sockaddr*) socket_config, len);
    return REQUEST_SUCESS;
}

short read_tnfs_UDP_packet(t_socket_fd UDP_socket_fd, t_tnfs_UDP_packet** ptr_tnfs_UDP_packet) {
    t_socket_config* client_config; bzero(client_config, sizeof(t_socket_config)); socklen_t len = sizeof(client_config);
    
    const int buffer_length = 2048;
    byte* bytes_buffer = malloc(sizeof(byte) * buffer_length); bzero(bytes_buffer, buffer_length);
    
    recvfrom(UDP_socket_fd, bytes_buffer, buffer_length, MSG_WAITALL, (struct sockaddr *) &client_config, &len);

    *ptr_tnfs_UDP_packet = deserialize_tnfs_UDP_packet(bytes_buffer);
    free(bytes_buffer); 

    return REQUEST_SUCESS; 
}