#include "tnfs_TCP_packet.h"

/* --- IMPORTS --- */
/* Common */
#include <stdlib.h>
#include <stdio.h>
/* Serialization */
#include "serialization.h"
#include "deserialization.h"
/* Typing */
#include "tnfs_packet_type.h"
#include "peersDAO.h"
#include "serialization_peers.h"
/* Utilitaries */
#include "hash32.h"
#include "uchecksum.h"
#include "random.h"
#include "uerror.h"
#include "upointer.h"
#include "string.h"
#include "ustring.h"

/* --- CODE --- */

const int TNFS_TCP_PACKET_HEADER_LENGTH = sizeof(hash32_t) + sizeof(hash32_t) + sizeof(t_tnfs_packet_type);
const int TNFS_TCP_PACKET_FOOTER_LENGTH = sizeof(hash32_t) + sizeof(int) + sizeof(int);

t_tnfs_TCP_packet* encapsulate_tnfs_TCP_packet(const hash32_t dest_node_id, const hash32_t src_node_id, const t_tnfs_packet_type type, const int payload_size, const void* payload, const hash32_t checksum, const int packet_number, const int session_id) {
    t_tnfs_TCP_packet* tnfs_TCP_packet = (t_tnfs_TCP_packet*) malloc(sizeof(t_tnfs_TCP_packet));
    
    tnfs_TCP_packet->dest_node_id = dest_node_id;
    tnfs_TCP_packet->src_node_id = src_node_id;

    tnfs_TCP_packet->type = type;
    tnfs_TCP_packet->payload_size = payload_size;
    tnfs_TCP_packet->payload = (void*) malloc(payload_size);
    memcpy(tnfs_TCP_packet->payload, payload, payload_size);

    tnfs_TCP_packet->checksum = checksum;

    tnfs_TCP_packet->packet_number = packet_number;
    tnfs_TCP_packet->session_id = session_id;

    return tnfs_TCP_packet;
}

void destroy_tnfs_TCP_packet(t_tnfs_TCP_packet* tnfs_TCP_packet) {
    free(tnfs_TCP_packet->payload);
    free(tnfs_TCP_packet);
}

byte* serialize_tnfs_TCP_packet(const t_tnfs_TCP_packet* tnfs_TCP_packet) {
    int* array_of_lengths = (int*) malloc(sizeof(int) * NUMBER_OF_FIELDS_OF_TNFS_TCP_PACKET);
    array_of_lengths[0] = sizeof(hash32_t);
    array_of_lengths[1] = sizeof(hash32_t);
    array_of_lengths[2] = sizeof(t_tnfs_packet_type);
    array_of_lengths[3] = sizeof(hash32_t);
    array_of_lengths[4] = sizeof(int);
    array_of_lengths[5] = sizeof(int);
    array_of_lengths[6] = sizeof(int);
    array_of_lengths[7] = tnfs_TCP_packet->payload_size;


    byte** array_of_byte_array = (byte**) malloc(sizeof(byte*) * NUMBER_OF_FIELDS_OF_TNFS_TCP_PACKET);
    array_of_byte_array[0] = (byte*) hash32_to_string(tnfs_TCP_packet->dest_node_id);
    array_of_byte_array[1] = (byte*) hash32_to_string(tnfs_TCP_packet->src_node_id);
    array_of_byte_array[2] = serialize_tnfs_packet_type(tnfs_TCP_packet->type);
    array_of_byte_array[3] = (byte*) hash32_to_string(tnfs_TCP_packet->checksum);
    array_of_byte_array[4] = integer_to_byte_array(tnfs_TCP_packet->packet_number);
    array_of_byte_array[5] = integer_to_byte_array(tnfs_TCP_packet->session_id);
    array_of_byte_array[6] = integer_to_byte_array(tnfs_TCP_packet->payload_size);
    array_of_byte_array[7] = serialize_tnfs_packet_payload(tnfs_TCP_packet->payload, tnfs_TCP_packet->payload_size, tnfs_TCP_packet->type);

    const int total_number_of_bytes = total_number_of_byte_of_tnfs_TCP_packet(tnfs_TCP_packet);
    int moved_bytes = 0;

    byte* tnfs_TCP_packet_bytes = (byte*) malloc(sizeof(byte) * total_number_of_bytes);
    for (int i = 0; i < NUMBER_OF_FIELDS_OF_TNFS_TCP_PACKET; i++) {
        concat_byte_arrays(tnfs_TCP_packet_bytes, array_of_byte_array[i], array_of_lengths[i], &moved_bytes);
        free(array_of_byte_array[i]);
    }

    free(array_of_byte_array);
    free(array_of_lengths);

    return tnfs_TCP_packet_bytes;
}

t_tnfs_TCP_packet* deserialize_tnfs_TCP_packet(byte* tnfs_TCP_packet_as_bytes) {
    byte* ptr = tnfs_TCP_packet_as_bytes;
    
    const hash32_t dest_node_id = string_to_hash32((hash32_char_t*) ptr);
    ptr += sizeof(hash32_t);
    const hash32_t src_node_id = string_to_hash32((hash32_char_t*) ptr);
    ptr += sizeof(hash32_t);

    t_tnfs_packet_type type = deserialize_tnfs_packet_type(ptr);
    ptr += sizeof(t_tnfs_packet_type);

    hash32_t checksum = string_to_hash32((hash32_char_t*) ptr);
    ptr += sizeof(hash32_t);

    int packet_number = byte_array_to_integer(ptr);
    ptr += sizeof(int);
    int session_id = byte_array_to_integer(ptr);
    ptr += sizeof(int);

    int payload_size = byte_array_to_integer(ptr);
    ptr += sizeof(int);
    void* payload = deserialize_tnfs_packet_payload(ptr, payload_size, type);
    ptr += payload_size;

    t_tnfs_TCP_packet* tnfs_TCP_packet = encapsulate_tnfs_TCP_packet(dest_node_id, src_node_id, type, payload_size, payload, checksum, packet_number, session_id);
    
    free(payload);
    
    return tnfs_TCP_packet;    
}

int total_number_of_byte_of_tnfs_TCP_packet(const t_tnfs_TCP_packet* tnfs_TCP_packet) {
    return TNFS_TCP_PACKET_HEADER_LENGTH + sizeof(int) + tnfs_TCP_packet->payload_size + TNFS_TCP_PACKET_FOOTER_LENGTH;
}

void print_tnfs_TCP_packet(const t_tnfs_TCP_packet* tnfs_TCP_packet) {
    fprintf(stdout, "tnfs_TCP_packet(&%p)={\n", tnfs_TCP_packet);
    fprintf(stdout, "\tdest_node_id:"); print_hash(tnfs_TCP_packet->dest_node_id); fprintf(stdout, "\n");
    fprintf(stdout, "\tsrc_node_id:"); print_hash(tnfs_TCP_packet->src_node_id); fprintf(stdout, "\n");
    fprintf(stdout, "\ttype:%d\n", tnfs_TCP_packet->type);
    fprintf(stdout, "\tpayload_size:%d\n", tnfs_TCP_packet->payload_size);
    fprintf(stdout, "\tpayload:...\n");
    fprintf(stdout, "\tchecksum:"); print_hash(tnfs_TCP_packet->checksum); fprintf(stdout, "\n");
    fprintf(stdout, "\tpacket_number:%d\n", tnfs_TCP_packet->packet_number);
    fprintf(stdout, "\tsession_id:%d\n", tnfs_TCP_packet->session_id);
    fprintf(stdout, "}\n");
}

t_tnfs_TCP_packet* create_ACK_tnfs_TCP_packet(const hash32_t dest_node, const hash32_t src_node) {
    int rstring_length = 0;
    char* rstring = random_string(&rstring_length, 4, 8);
    t_tnfs_TCP_packet* tnfs_TCP_packet = encapsulate_tnfs_TCP_packet(dest_node, src_node, F_TNFS_ACK, rstring_length, rstring, compute_checksum(rstring, rstring_length), 0, 0);
    free(rstring);
    return tnfs_TCP_packet;
}

t_tnfs_TCP_packet* create_SYN_tnfs_TCP_packet(const hash32_t dest_node, const hash32_t src_node) {
    int rstring_length = 0;
    char* rstring = random_string(&rstring_length, 4, 8);
    t_tnfs_TCP_packet* tnfs_TCP_packet = encapsulate_tnfs_TCP_packet(dest_node, src_node, F_TNFS_SYN, rstring_length, rstring, compute_checksum(rstring, rstring_length), 0, 0);
    free(rstring);
    return tnfs_TCP_packet;
}

t_tnfs_TCP_packet* create_END_SYN_tnfs_TCP_packet(const hash32_t dest_node, const hash32_t src_node) {
    int rstring_length = 0;
    char* rstring = random_string(&rstring_length, 4, 8);
    t_tnfs_TCP_packet* tnfs_TCP_packet = encapsulate_tnfs_TCP_packet(dest_node, src_node, F_TNFS_SYN, rstring_length, rstring, compute_checksum(rstring, rstring_length), 0, 0);
    free(rstring);
    return tnfs_TCP_packet;
}

t_tnfs_TCP_packet* create_RETRY_tnfs_TCP_packet(const hash32_t dest_node, const hash32_t src_node, const int block_number) {
    int block_number_as_string_length;
    char* block_number_as_string = integer_to_string(block_number, &block_number_as_string_length);
    t_tnfs_TCP_packet* tnfs_TCP_packet = encapsulate_tnfs_TCP_packet(dest_node, src_node, F_TNFS_RETRY, sizeof(int), &block_number, compute_checksum(block_number_as_string, block_number_as_string_length), 0, 0);
    free(block_number_as_string);
    return tnfs_TCP_packet;
}

t_tnfs_TCP_packet* create_ERROR_DURING_OPERATION_tnfs_TCP_packet(const hash32_t dest_node, const hash32_t src_node, const int error_code) {
    int error_code_as_string_length;
    char* error_code_as_string = integer_to_string((int) error_code, &error_code_as_string_length);
    t_tnfs_TCP_packet* tnfs_TCP_packet = encapsulate_tnfs_TCP_packet(dest_node, src_node, F_TNFS_ERROR_DURING_OPERATION, sizeof(int), &error_code, compute_checksum(error_code_as_string, error_code_as_string_length), 0, 0);
    free(error_code_as_string);
    return tnfs_TCP_packet;
}

t_tnfs_TCP_packet* create_DOWNLOAD_tnfs_TCP_packet(const hash32_t dest_node, const hash32_t src_node, const hash32_t file_cid) {
    hash32_char_t* hash_as_string = hash32_to_string(file_cid);
    t_tnfs_TCP_packet* tnfs_TCP_packet = encapsulate_tnfs_TCP_packet(dest_node, src_node, F_TNFS_DOWNLOAD, sizeof(hash32_char_t) * 4, &file_cid, compute_checksum(hash_as_string, 4), 0, 0);
    free(hash_as_string);
    return tnfs_TCP_packet;
}

t_tnfs_TCP_packet* create_DOWNLOAD_END_tnfs_TCP_packet(const hash32_t dest_node, const hash32_t src_node) {
    int rstring_length = 0;
    char* rstring = random_string(&rstring_length, 4, 8);
    t_tnfs_TCP_packet* tnfs_TCP_packet = encapsulate_tnfs_TCP_packet(dest_node, src_node, F_TNFS_DOWNLOAD_END, rstring_length, rstring, compute_checksum(rstring, rstring_length), 0, 0);
    free(rstring);
    return tnfs_TCP_packet;
}

t_tnfs_TCP_packet* create_PUBLISH_tnfs_TCP_packet(const hash32_t dest_node, const hash32_t src_node, char* file_name, int file_name_length) {
    hash32_char_t* hash_as_string = hash32_to_string(hash32(file_name, file_name_length));
    t_tnfs_TCP_packet* tnfs_TCP_packet = encapsulate_tnfs_TCP_packet(dest_node, src_node, F_TNFS_PUBLISH, file_name_length, file_name, compute_checksum(hash_as_string, 4), 0, 0);
    free(hash_as_string);
    return tnfs_TCP_packet;
}

t_tnfs_TCP_packet* create_PUBLISH_END_tnfs_TCP_packet(const hash32_t dest_node, const hash32_t src_node) {
    int rstring_length = 0;
    char* rstring = random_string(&rstring_length, 4, 8);
    t_tnfs_TCP_packet* tnfs_TCP_packet = encapsulate_tnfs_TCP_packet(dest_node, src_node, F_TNFS_PUBLISH_END, rstring_length, rstring, compute_checksum(rstring, rstring_length), 0, 0);
    free(rstring);
    return tnfs_TCP_packet;
}

t_tnfs_TCP_packet* create_FILE_BLOCK_tnfs_TCP_packet(const hash32_t dest_node, const hash32_t src_node, byte* block, const int block_length) {
    return encapsulate_tnfs_TCP_packet(dest_node, src_node, F_TNFS_FILE_BLOCK, block_length, block, compute_checksum(block, block_length), 0, 0);
}

t_tnfs_TCP_packet* create_GET_PEERS_tnfs_TCP_packet(const hash32_t dest_node, const hash32_t src_node) {
    int rstring_length = 0;
    char* rstring = random_string(&rstring_length, 4, 8);
    t_tnfs_TCP_packet* tnfs_TCP_packet = encapsulate_tnfs_TCP_packet(dest_node, src_node, F_TNFS_GET_PEERS, rstring_length, rstring, compute_checksum(rstring, rstring_length), 0, 0);
    free(rstring);
    return tnfs_TCP_packet;
}

t_tnfs_TCP_packet* create_TNFS_GET_PEERS_RESPONSE_TCP_packet(const hash32_t dest_node, const hash32_t src_node, Peer* peer) {
    const int total_number_of_byte = total_number_of_byte_of_peer(peer) + sizeof(int);
    byte* serialized_peer = serialize_peer(peer);
    t_tnfs_TCP_packet* packet = encapsulate_tnfs_TCP_packet(dest_node, src_node, F_TNFS_GET_PEERS_RESPONSE, total_number_of_byte, peer, compute_checksum(serialized_peer, total_number_of_byte), 0, 0);
    free(serialized_peer);
    return packet;
}

t_tnfs_TCP_packet* create_GET_PEERS_END_tnfs_TCP_packet(const hash32_t dest_node, const hash32_t src_node) {
    int rstring_length = 0;
    char* rstring = random_string(&rstring_length, 4, 8);
    t_tnfs_TCP_packet* tnfs_TCP_packet = encapsulate_tnfs_TCP_packet(dest_node, src_node, F_TNFS_GET_PEERS_END, rstring_length, rstring, compute_checksum(rstring, rstring_length), 0, 0);
    free(rstring);
    return tnfs_TCP_packet;
}
