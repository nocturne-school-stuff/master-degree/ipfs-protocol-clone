#include "tnfs_packet_type.h"

/* --- IMPORTS --- */
/* Serialization */
#include "serialization.h"
#include "deserialization.h"
#include "peersDAO.h"
#include "serialization_peers.h"
/* Utilitaries */
#include "hash32.h"
#include "fs_api.h"
#include "ustring.h"
#include "upointer.h"

/* --- CODE --- */

byte* serialize_tnfs_packet_type(const t_tnfs_packet_type tnfs_packet_type) {
    byte* tnfs_packet_type_byte = (byte*) malloc(sizeof(byte));
    *tnfs_packet_type_byte = char_to_byte(tnfs_packet_type);
    return tnfs_packet_type_byte;
} 

t_tnfs_packet_type deserialize_tnfs_packet_type(const byte* tnfs_packet_type_as_byte) {
    return byte_to_char(*tnfs_packet_type_as_byte);
}

byte* serialize_tnfs_packet_payload(const void* payload, const int payload_size, const t_tnfs_packet_type payload_type) {
    switch (payload_type) {
        case F_TNFS_RETRY : case F_TNFS_ERROR_DURING_OPERATION : return integer_to_byte_array(*((int*) payload));
        case F_TNFS_ACK : case F_TNFS_SYN : case F_TNFS_DOWNLOAD_END : case F_TNFS_PUBLISH_END : case F_TNFS_PING : case F_TNFS_PING_RESPONSE : case F_TNFS_GET_PEERS : case F_TNFS_GET_PEERS_END : return char_array_to_byte_array(payload, payload_size) ;
        case F_TNFS_FILE_BLOCK: return serialize_file_block(payload, payload_size);
        case F_TNFS_DOWNLOAD : case F_TNFS_FIND_FILE : case F_TNFS_FIND_FILE_RESPONSE : return (byte*) hash32_to_string(*((hash32_t*) payload));
        case F_TNFS_GET_PEERS_RESPONSE : return serialize_peer((Peer*) payload);
        case F_TNFS_PUBLISH : return char_array_to_byte_array((char*) payload, payload_size);
        default: return NULL;
    }
}

void* deserialize_tnfs_packet_payload(const byte* payload_as_bytes, const int payload_size, const t_tnfs_packet_type payload_type) {
    switch (payload_type) {
        case F_TNFS_RETRY : case F_TNFS_ERROR_DURING_OPERATION : return int_pointer(byte_array_to_integer(payload_as_bytes));
        case F_TNFS_ACK : case F_TNFS_SYN : case F_TNFS_DOWNLOAD_END : case F_TNFS_PUBLISH_END : case F_TNFS_PING : case F_TNFS_PING_RESPONSE : case F_TNFS_GET_PEERS : case F_TNFS_GET_PEERS_END : return byte_array_to_char_array(payload_as_bytes, payload_size) ;
        case F_TNFS_FILE_BLOCK: return deserialize_file_block(payload_as_bytes, payload_size);
        case F_TNFS_DOWNLOAD : case F_TNFS_FIND_FILE : case F_TNFS_FIND_FILE_RESPONSE : return (hash32_t*) int_pointer(string_to_hash32((char*) payload_as_bytes));
        case F_TNFS_GET_PEERS_RESPONSE : return deserialize_peer(payload_as_bytes);
        case F_TNFS_PUBLISH : return byte_array_to_char_array(payload_as_bytes, payload_size);
        default: return NULL;
    }
}