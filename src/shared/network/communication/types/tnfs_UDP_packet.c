#include "tnfs_UDP_packet.h"

/* --- IMPORTS --- */
/* Common */
#include <stdlib.h>
#include <stdio.h>
/* Serialization */
#include "serialization.h"
#include "deserialization.h"
/* Typing */
#include "tnfs_packet_type.h"
/* Utilitaries */
#include "hash32.h"
#include "uerror.h"
#include "upointer.h"
#include "string.h"
#include "ustring.h"

/* --- CODE --- */

const int TNFS_UDP_PACKET_HEADER_LENGTH = sizeof(hash32_t) + sizeof(hash32_t) + sizeof(t_tnfs_packet_type) + sizeof(int); 
const int TNFS_UDP_PACKET_FOOTER_LENGTH = 0;

t_tnfs_UDP_packet* encapsulate_tnfs_UDP_packet(const hash32_t dest_node_id, const hash32_t src_node_id, const t_tnfs_packet_type type, const int payload_size, const void* payload) {
    t_tnfs_UDP_packet* tnfs_UDP_packet = (t_tnfs_UDP_packet*) malloc(sizeof(t_tnfs_UDP_packet));
    
    tnfs_UDP_packet->dest_node_id = dest_node_id;
    tnfs_UDP_packet->src_node_id = src_node_id;

    tnfs_UDP_packet->type = type;
    tnfs_UDP_packet->payload_size = payload_size;
    tnfs_UDP_packet->payload = (void*) malloc(payload_size);
    memcpy(tnfs_UDP_packet->payload, payload, payload_size);

    return tnfs_UDP_packet;
}

void destroy_tnfs_UDP_packet(t_tnfs_UDP_packet* tnfs_UDP_packet) {
    free(tnfs_UDP_packet->payload);
    free(tnfs_UDP_packet);
}

byte* serialize_tnfs_UDP_packet(const t_tnfs_UDP_packet* tnfs_UDP_packet) {
    int* array_of_lengths = (int*) malloc(sizeof(int) * NUMBER_OF_FIELDS_OF_TNFS_UDP_PACKET);
    array_of_lengths[0] = sizeof(hash32_t);
    array_of_lengths[1] = sizeof(hash32_t);
    array_of_lengths[2] = sizeof(t_tnfs_packet_type);
    array_of_lengths[3] = sizeof(int);
    array_of_lengths[4] = tnfs_UDP_packet->payload_size;

    byte** array_of_byte_array = (byte**) malloc(sizeof(byte*) * NUMBER_OF_FIELDS_OF_TNFS_UDP_PACKET);
    array_of_byte_array[0] = (byte*) hash32_to_string(tnfs_UDP_packet->dest_node_id);
    array_of_byte_array[1] = (byte*) hash32_to_string(tnfs_UDP_packet->src_node_id);
    array_of_byte_array[2] = serialize_tnfs_packet_type(tnfs_UDP_packet->type);
    array_of_byte_array[3] = integer_to_byte_array(tnfs_UDP_packet->payload_size);
    array_of_byte_array[4] = serialize_tnfs_packet_payload(tnfs_UDP_packet->payload, tnfs_UDP_packet->payload_size, tnfs_UDP_packet->type);

    const int total_number_of_bytes = total_number_of_byte_of_tnfs_UDP_packet(tnfs_UDP_packet);
    int moved_bytes = 0;

    byte* tnfs_UDP_packet_bytes = (byte*) malloc(sizeof(byte) * total_number_of_bytes);
    for (int i = 0; i < NUMBER_OF_FIELDS_OF_TNFS_UDP_PACKET; i++) {
        concat_byte_arrays(tnfs_UDP_packet_bytes, array_of_byte_array[i], array_of_lengths[i], &moved_bytes);
        free(array_of_byte_array[i]);
    }

    free(array_of_byte_array);
    free(array_of_lengths);

    return tnfs_UDP_packet_bytes;
}

t_tnfs_UDP_packet* deserialize_tnfs_UDP_packet(byte* tnfs_UDP_packet_as_bytes) {
    byte* ptr = tnfs_UDP_packet_as_bytes;
    
    const hash32_t dest_node_id = string_to_hash32((hash32_char_t*) ptr);
    ptr += sizeof(hash32_t);
    const hash32_t src_node_id = string_to_hash32((hash32_char_t*) ptr);
    ptr += sizeof(hash32_t);
    
    t_tnfs_packet_type type = deserialize_tnfs_packet_type(ptr);
    ptr += sizeof(t_tnfs_packet_type);
    int payload_size = byte_array_to_integer(ptr);
    ptr += sizeof(int);
    void* payload = deserialize_tnfs_packet_payload(ptr, payload_size, type);
    ptr += payload_size;

    t_tnfs_UDP_packet* tnfs_UDP_packet = encapsulate_tnfs_UDP_packet(dest_node_id, src_node_id, type, payload_size, payload);

    free(payload);

    return tnfs_UDP_packet;    
}

int total_number_of_byte_of_tnfs_UDP_packet(const t_tnfs_UDP_packet* tnfs_UDP_packet) {
    int total_number_of_byte = 0;

    total_number_of_byte += sizeof(hash32_t);
    total_number_of_byte += sizeof(hash32_t);
    total_number_of_byte += sizeof(t_tnfs_packet_type);
    total_number_of_byte += sizeof(int);
    total_number_of_byte += tnfs_UDP_packet->payload_size;

    return total_number_of_byte;
}

void print_tnfs_UDP_packet(const t_tnfs_UDP_packet* tnfs_UDP_packet) {
    fprintf(stdout, "tnfs_UDP_packet(&%p)={\n", tnfs_UDP_packet);
    fprintf(stdout, "\tdest_node_id:"); print_hash(tnfs_UDP_packet->dest_node_id); fprintf(stdout, "\n");
    fprintf(stdout, "\tsrc_node_id:"); print_hash(tnfs_UDP_packet->src_node_id); fprintf(stdout, "\n");
    fprintf(stdout, "\ttype:%d\n", tnfs_UDP_packet->type);
    fprintf(stdout, "\tpayload_size:%d\n", tnfs_UDP_packet->payload_size);
    fprintf(stdout, "\tpayload:...\n");
    fprintf(stdout, "}\n");
}

t_tnfs_UDP_packet* create_PING_tnfs_UDP_packet(hash32_t dest_node, hash32_t src_node, char* full_ip_addr) {
    return encapsulate_tnfs_UDP_packet(dest_node, src_node, F_TNFS_PING, strnlen(full_ip_addr, 21), full_ip_addr);
}

t_tnfs_UDP_packet* create_PING_RESPONSE_tnfs_UDP_packet(hash32_t dest_node, hash32_t src_node, char* full_ip_addr) {
    return encapsulate_tnfs_UDP_packet(dest_node, src_node, F_TNFS_PING_RESPONSE, strnlen(full_ip_addr, 21), full_ip_addr);
}

t_tnfs_UDP_packet* create_FIND_FILE_tnfs_UDP_packet(hash32_t dest_node, hash32_t src_node, hash32_t file_cid) {
    hash32_char_t* hash_as_string = hash32_to_string(file_cid);
    t_tnfs_UDP_packet* packet = encapsulate_tnfs_UDP_packet(dest_node, src_node, F_TNFS_FIND_FILE, sizeof(hash32_char_t) * 4, hash_as_string);
    free(hash_as_string);
    return packet;
}

t_tnfs_UDP_packet* create_FIND_FILE_RESPONSE_tnfs_UDP_packet(hash32_t dest_node, hash32_t src_node, hash32_t file_cid) {
    hash32_char_t* hash_as_string = hash32_to_string(file_cid);
    t_tnfs_UDP_packet* packet = encapsulate_tnfs_UDP_packet(dest_node, src_node, F_TNFS_FIND_FILE_RESPONSE, sizeof(hash32_char_t) * 4, hash_as_string);
    free(hash_as_string);
    return packet;
}