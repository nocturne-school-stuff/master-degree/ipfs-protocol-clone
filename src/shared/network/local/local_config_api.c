#include "local_config_api.h"

/* --- IMPORTS --- */
/* iostream */
#include "fs_api.h"
#include <unistd.h>
/* Globals */
#include "globals.h"
/* Typing */
#include "hash32.h"
/* Utilitaries */
#include <string.h>

/* --- CODE --- */
short load_local_config() {
    file_fd_t local_config_fd = open_file(LOCAL_CONFIG_FILE_PATH);
    if (local_config_fd == INVALID_FILE_DESCRIPTOR) return COULDNT_LOAD_LOCAL_CONFIG;

    char buffer[LOCAL_CONFIG_LOADING_BUFFER_LENGTH];
    bzero(buffer, LOCAL_CONFIG_LOADING_BUFFER_LENGTH);

    int bytes_read = read(local_config_fd, buffer, LOCAL_CONFIG_LOADING_BUFFER_LENGTH);
    if (bytes_read == -1) return COULDNT_LOAD_LOCAL_CONFIG;
    
    close_file(local_config_fd);

    return LOCAL_CONFIG_LOADED;
}

short create_local_config() {
    file_fd_t local_config_fd = create_file(LOCAL_CONFIG_FILE_PATH);
    if (local_config_fd == INVALID_FILE_DESCRIPTOR) return LOCAL_CONFIG_CREATION_FAILURE;

    G_local_node_id = random_hash32();
    hash32_char_t* G_local_node_id_as_string = hash32_to_string(G_local_node_id);
    if (write(local_config_fd, G_local_node_id_as_string, sizeof(hash32_t)) == -1) {
        free(G_local_node_id_as_string);
        return LOCAL_CONFIG_CREATION_FAILURE;
    } 
    close_file(local_config_fd);
    
    return LOCAL_CONFIG_CREATION_SUCCESS;
}