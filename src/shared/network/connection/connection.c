#include "connection.h"

/* --- IMPORTS --- */
/* Common */
#include <stdlib.h>
/* Networking */
#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
/* Utilitaries */
#include <string.h>
#include <ustring.h>
#include "uerror.h"

/* --- CODE --- */
t_socket_config* get_socket_config_for(const char* dest_addr, const short port_num) {
    t_socket_config* configuration = (t_socket_config*) malloc(sizeof(t_socket_config));
    
    bzero(configuration, sizeof(t_socket_config));

    configuration->sin_family = AF_INET;
    configuration->sin_addr.s_addr = (dest_addr != NULL) ? inet_addr(dest_addr) : htonl(INADDR_ANY);
    configuration->sin_port = htons(port_num);

    return configuration;
}

t_socket_config* get_server_config_on(const short port_num) {
    return get_socket_config_for(NULL, port_num);
}

t_socket_fd create_raw_tcp_socket(const short socket_protocol) {
    if (!(socket_protocol == TCP || socket_protocol == UDP)) {
        error_message("unknown socket protocol specified!");
        return INVALID_SOCKET_FD;
    }

    t_socket_fd socket_fd = socket(AF_INET, (socket_protocol == TCP) ? SOCK_STREAM : SOCK_DGRAM, 0);

    if (socket_fd < 0) {
        error_message("socket creation failed!");   
        return INVALID_SOCKET_FD;
    }

    return socket_fd;
}

t_socket_fd create_server_socket(t_socket_config* configuration, const short socket_protocol) {
    t_socket_fd socket_fd = create_raw_tcp_socket(socket_protocol);

    setsockopt(socket_fd, SOL_SOCKET, SO_REUSEADDR, &socket_fd, sizeof(socket_fd));
    if ((bind(socket_fd, (struct sockaddr*) configuration, sizeof(*configuration))) != 0) {
        error_message("socket binding failed!");
        return INVALID_SOCKET_FD;
    }

    return socket_fd;
}

t_socket_fd create_client_socket(t_socket_config* configuration, const short socket_protocol) {
    t_socket_fd socket_fd = create_raw_tcp_socket(socket_protocol);

    if (connect(socket_fd, (struct sockaddr*) configuration, sizeof(*configuration)) != 0) {
        error_message("socket connection failed!");
        return INVALID_SOCKET_FD;
    }

    return socket_fd;
}

short close_socket(t_socket_fd socket_fd) {
    return (short) shutdown(socket_fd, SHUT_RDWR) ? ERROR_CLOSING_SOCKET : SOCKET_CLOSED;
}