#include "fs_api.h"

/* --- IMPORTS --- */
/* IO streams */
#include <unistd.h>
#include <fcntl.h>
/* Serialization */
#include "serialization.h"
#include <stdio.h>
/* Utilitaries */
#include <string.h>

/* --- CODE --- */

short verify_file_path(char* file_path) {
    return (access(file_path, F_OK) != -1) ? FILE_PATH_EXIST : FILE_PATH_INVALID;
}

char* get_file_name_from_file_path(char* file_path, int file_path_length, int* file_name_ptr_length) {
    char* file_name = strrchr(file_path, '/');
    *file_name_ptr_length = strnlen(file_name, file_path_length);
    return file_name;
}

file_fd_t open_file(const char* file_path) {
    return open(file_path, O_RDONLY);
}

byte* read_next_block(file_fd_t file_fd, int* bytes_read) {
    byte* block = (byte*) malloc(sizeof(byte) * BLOCK_DIGEST_LENGTH);

    *bytes_read = read(file_fd, block, BLOCK_DIGEST_LENGTH);

    return block;
}

file_fd_t create_file(const char* file_path) {
    return open(file_path, O_CREAT | O_WRONLY, S_IRUSR | S_IWUSR);
}

file_fd_t overwrite_file(const char* file_path) {
    return open(file_path, O_TRUNC | O_WRONLY, S_IRUSR | S_IWUSR);
}

short unlink_file(const char* file_path) {
    return unlink(file_path) ? DELETE_ERROR : DELETE_SUCCESS;
}

int write_block_in_file(file_fd_t file_fd, byte* to_write, const short to_write_length) {
    return write(file_fd, to_write, to_write_length);
}

short close_file(file_fd_t file_fd) {
    return close(file_fd) ? ERROR_DURING_CLOSING : CLOSING_SUCCESS;
}

byte* serialize_file_block(const byte* file_block, const int file_block_length) {
    byte* serialized = copy_byte_array(file_block, file_block_length);
    serialized[file_block_length] = 0xFF;
    return serialized;
}

byte* deserialize_file_block(const byte* serialized_file_block, const int file_block_length) {
    byte* deserialized = copy_byte_array(serialized_file_block, file_block_length);
    deserialized[file_block_length] = 0x00;
    return deserialized;
}