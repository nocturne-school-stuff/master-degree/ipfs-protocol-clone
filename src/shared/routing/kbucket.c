#include "kbucket.h"

/* --- IMPORTS --- */
/* Globals */
#include "globals.h"
/* Math */
#include <math.h>
/* Utilitaries */
#include <string.h>
#include <stdio.h>

/* --- CODE --- */

Bucket* init_bucket() {
    Bucket* bucket = malloc(sizeof(Bucket));
    bzero(bucket, sizeof(bucket));
    bucket->peer = NULL;
    bucket->subBuckets[0] = NULL;
    bucket->subBuckets[1] = NULL;
    return bucket;
}

BucketRow* init_bucket_row(unsigned long min_range, unsigned long max_range) {
    BucketRow* bucketRow = malloc(sizeof(BucketRow));
    bzero(bucketRow, sizeof(BucketRow));
    bucketRow->min_range = min_range;
    bucketRow->max_range = max_range;
    bucketRow->value = init_bucket();
    return bucketRow;
}

void add_peer_to_bucket_row(BucketRow* bucketRow, Peer* peer) {
    if (bucketRow->value->peer == NULL && bucketRow->value->subBuckets[0] == NULL && bucketRow->value->subBuckets[1] == NULL) {
        bucketRow->value->peer = peer;
        return;
    }

    hash32_t bucket_middle_distance = (bucketRow->min_range + bucketRow->max_range) / 2;

    if (bucketRow->value->subBuckets[0] == NULL) bucketRow->value->subBuckets[0] = init_bucket_row(bucketRow->min_range, bucket_middle_distance);
    if (bucketRow->value->subBuckets[1] == NULL) bucketRow->value->subBuckets[1] = init_bucket_row(bucket_middle_distance + 1, bucketRow->max_range);

    if (bucketRow->value->peer != NULL) {
        if (xor_metric(string_to_hash32(bucketRow->value->peer->peer_id), G_local_node_id) <= bucket_middle_distance) add_peer_to_bucket_row(bucketRow->value->subBuckets[0], bucketRow->value->peer);
        else add_peer_to_bucket_row(bucketRow->value->subBuckets[1], bucketRow->value->peer);

        bucketRow->value->peer = NULL;
    }

    if (xor_metric(string_to_hash32(peer->peer_id), G_local_node_id) <= bucket_middle_distance) add_peer_to_bucket_row(bucketRow->value->subBuckets[0], peer);
    else add_peer_to_bucket_row(bucketRow->value->subBuckets[1], peer);
}

Peer* find_peer(BucketRow* bucketRow, hash32_t peer_cid) {
    if (bucketRow->value->peer == NULL && bucketRow->value->subBuckets[0] == NULL && bucketRow->value->subBuckets[1] == NULL) return NULL;
    
    if (bucketRow->value->peer != NULL) {
        if (string_to_hash32(bucketRow->value->peer->peer_id) == peer_cid) return bucketRow->value->peer;
        else return NULL;
    }

    if (xor_metric(peer_cid, G_local_node_id) <= bucketRow->value->subBuckets[0]->max_range) return find_peer(bucketRow->value->subBuckets[0], peer_cid);
    else return find_peer(bucketRow->value->subBuckets[1], peer_cid);
}

Peer* find_nearest_peer(BucketRow* bucketRow, hash32_t peer_cid) {
    if (bucketRow->value->peer == NULL && bucketRow->value->subBuckets[0] == NULL && bucketRow->value->subBuckets[1] == NULL) return NULL;
    if (bucketRow->value->peer != NULL) return bucketRow->value->peer;
    
    Peer* left_search_peer = find_nearest_peer(bucketRow->value->subBuckets[0], peer_cid);
    Peer* right_peer_search = find_nearest_peer(bucketRow->value->subBuckets[1], peer_cid);

    if (left_search_peer == NULL) return right_peer_search;
    else if (right_peer_search == NULL) return left_search_peer;
    else {
        unsigned long left_distance = xor_metric(string_to_hash32(left_search_peer->peer_id), peer_cid);
        unsigned long right_distance = xor_metric(string_to_hash32(right_peer_search->peer_id), peer_cid);

        return (left_distance < right_distance) ? left_search_peer : right_peer_search;
    }
}