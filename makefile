CC = gcc
BD = ./bin/
SC = ./src/
CFLAGS = -g -O -I ./includes
LFLAGS = -lpthread -lssl -lcrypto -lsqlite3 -lm

define OBJ
main
test
server/TCPServer
server/UDPServer
client/client
client/interface
shared/multithreading/multithreading_api
shared/request/tcp/TCP_request_api
shared/request/tcp/TCP_procedures
shared/request/udp/UDP_procedures
shared/routing/kbucket
shared/network/network_api 
shared/network/communication/transmission
shared/network/connection/connection 
shared/network/communication/types/tnfs_TCP_packet 
shared/network/communication/types/tnfs_UDP_packet 
shared/network/communication/types/tnfs_packet_type
shared/fs/fs_api
shared/network/communication/serialization/deserialization  
shared/network/communication/serialization/serialization
shared/db/filesDAO
shared/db/peersDAO
shared/db/serialization_files
shared/db/serialization_peers
shared/network/local/local_config_api
shared/db/db_functions 
shared/globals
shared/utils/uchecksum
shared/utils/hash32
shared/utils/random
shared/utils/timeout
shared/utils/uerror 
shared/utils/upointer  
shared/utils/ustring 
endef

define DEPENDENCIES
libssl-dev
libsqlite3-dev
endef

reverse = $(if $(1),$(call reverse,$(wordlist 2,$(words $(1)),$(1)))) $(firstword $(1))

EXEC = tnfs.out

all : compile_$(EXEC)

compile_$(EXEC) : compile_obj
	$(CC) -o $(EXEC) $(addprefix $(BD), $(addsuffix .o, $(notdir $(OBJ)))) $(LFLAGS);

compile_obj : create_bin_dir
	$(foreach _OBJ, $(call reverse, $(OBJ)), \
		$(CC) $(CFLAGS) -c $(SC)$(_OBJ).c -o $(BD)$(notdir $(_OBJ).o);)

create_bin_dir :
	[ -d $(BD) ] || mkdir -p $(BD)

install :
	apt-get update -y;
	$(foreach _DEPENDENCY, $(DEPENDENCIES), \
		apt-get install -y $(_DEPENDENCY); \
	)

run: 
	./$(EXEC);

bootstrap:
	./$(EXEC) -s -p 8080

test:
	./$(EXEC) --test-mode;
	
clean: 
	rm -rf ./bin/ && rm -rf $(EXEC) && rm local.conf && rm local_database.db
