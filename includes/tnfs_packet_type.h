#ifndef H_tnfs_packet_type
#define H_tnfs_packet_type

/* --- IMPORTS --- */
/* Common */
#include <stdlib.h>
/* Serialization */
#include "serialization.h"

/* --- CODE --- */
/**
 * @brief The max length a node id can be
 */
#define CID_LENGTH 4
/**
 * @brief The length of the checksum
 */
#define CHECKSUM_LENGTH CID_LENGTH

/**
 * @brief tnfs packets types constants
 */

/* TCP FLAGS */

#define F_TNFS_ACK 0x00
#define F_TNFS_SYN 0x01
#define F_TNFS_END_SYN 0x02
#define F_TNFS_RETRY 0x03
#define F_TNFS_ERROR_DURING_OPERATION 0x04

#define F_TNFS_DOWNLOAD 0x10
#define F_TNFS_DOWNLOAD_END 0x11
#define F_TNFS_PUBLISH 0x12
#define F_TNFS_PUBLISH_END 0x13
#define F_TNFS_FILE_BLOCK 0x14

#define F_TNFS_GET_PEERS 0x20
#define F_TNFS_GET_PEERS_RESPONSE 0x21
#define F_TNFS_GET_PEERS_END 0x22

/* UDP FLAGS */

#define F_TNFS_PING 0x30
#define F_TNFS_PING_RESPONSE 0x31

#define F_TNFS_FIND_FILE 0x40
#define F_TNFS_FIND_FILE_RESPONSE 0x41


/**
 * @brief Error code constants
 */
#define RESSOURCE_NOT_FOUND 404
#define PROCEDURE_ABORTION 500

/**
 * @brief Represent a tnfs packets flag
 */
typedef u_int8_t t_tnfs_packet_type;

/**
 * @brief Cast tnfs_packet_type type into a byte
 * 
 * @param tnfs_packet_type The tnfs_packet_type to cast
 * 
 * @return the tnfs_packet_type as a byte
 */
byte* serialize_tnfs_packet_type(const t_tnfs_packet_type tnfs_packet_type);

/**
 * @brief Cast a byte array into a tnfs_packet_type byte
 * 
 * @param tnfs_packet_type_as_byte the tnfs_packet_type as a byte
 * 
 * @return the tnfs_packet_type
 */
t_tnfs_packet_type deserialize_tnfs_packet_type(const byte* tnfs_packet_type_as_byte);

/**
 * @brief Cast a tnfs_packet payload object to a byte array
 * 
 * @param payload the tnfs_packet payload
 * @param payload_size the tnfs_packet payload size
 * @param payload_type the type of the tnfs_packet payload
 * 
 * @return a tnfs_packet payload as a byte array 
 */
byte* serialize_tnfs_packet_payload(const void* payload, const int payload_size, const t_tnfs_packet_type payload_type);

/**
 * @brief Cast a byte array into a tnfs_packet payload object
 * 
 * @param payload_as_bytes the tnfs_packet payload as a byte array
 * @param payload_size the tnfs_packet payload size
 * @param payload_type the type of the tnfs_packet payload
 * 
 * @return a tnfs_packet payload object
 */
void* deserialize_tnfs_packet_payload(const byte* payload_as_bytes, const int payload_size, const t_tnfs_packet_type payload_type);

#endif // tnfs_packet_type