#ifndef H_UDP_procedures
#define H_UDP_procedures

/* --- IMPORTS --- */
/* Typing */
#include "tnfs_UDP_packet.h"
/* --- CODE --- */
#define UDP_PROCEDURE_SUCESS 0
#define UDP_PROCEDURE_FAILURE 1

/**
 * @brief handle an UDP tnfs ping request
 * 
 * @param packet the packet to handle
 * 
 * @return a flag describing the result of the procedure
 */
short handle_ping_procedure(t_tnfs_UDP_packet* packet);

/**
 * @brief handle an UDP tnfs ping response request
 * 
 * @param packet the packet to handle
 * 
 * @return a flag describing the result of the procedure
 */
short handle_ping_response_procedure(t_tnfs_UDP_packet* packet);

/**
 * @brief handle an UDP tnfs find file request
 * 
 * @param packet the packet to handle
 * 
 * @return a flag describing the result of the procedure
 */
short handle_find_file_procedure(t_tnfs_UDP_packet* packet);

/**
 * @brief handle an UDP tnfs find file request
 * 
 * @param packet the packet to handle
 * 
 * @return a flag describing the result of the procedure
 */
short handle_find_file_reponse_procedure(t_tnfs_UDP_packet* packet);

#endif // UDP_procedure