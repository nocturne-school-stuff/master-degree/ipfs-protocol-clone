#ifndef H_fs_api
#define H_fs_api

/* --- IMPORTS --- */
/* Serialization */
#include "serialization.h"

/* --- CODE --- */

/**
 * @brief A shortcut for file descriptor type
 */
typedef int file_fd_t;

#define INVALID_FILE_DESCRIPTOR -1

/**
 * Define the ideal length of a block
 */
#define BLOCK_DIGEST_LENGTH 16384

#define FILE_PATH_EXIST 1
#define FILE_PATH_INVALID 0

short verify_file_path(char* file_path);

char* get_file_name_from_file_path(char* file_path, int file_path_length, int* file_name_ptr_length);

/**
 * @brief Open the file pointed by the path in read mode
 * 
 * @param file_path the path of the file
 * 
 * @return a file descriptor to the file or NULL if the file hasnt been found
 */
file_fd_t open_file(const char* file_path);

/**
 * A const that indicate if the end of the file has been reached when reading it
 */
#define EOF_REACHED 0

/**
 * @brief Read the next block of the file
 * 
 * @param file_fd the file descriptor of the file
 * @param bytes_read a pointer to a short for saving the number of bytes read
 * 
 * @return the next block in the file as a byte array
 */
byte* read_next_block(file_fd_t file_fd, int* bytes_read);

/**
 * @brief Create a new file and open it in write mode
 * 
 * @param file_path the path of the file
 * 
 * @return the file descriptor of the newly created file or NULL if file could not have been created
 */
file_fd_t create_file(const char* file_path);

/**
 * @brief Create a new file that overwrite an existing one and open it in write mode
 * 
 * @param file_path the path of the file
 * 
 * @return the file descriptor of the newly created file or NULL if file could not have been created
 */
file_fd_t overwrite_file(const char* file_path);

#define DELETE_ERROR 0
#define DELETE_SUCCESS 1

/**
 * @brief Delete a file
 * 
 * @param file_path the path of the file
 * 
 */
short unlink_file(const char* file_path);

/**
 * @brief write a block of byte in the file pointed by the file descriptor
 * 
 * @param file_fd the file descriptor of the file
 * @param to_write the block of byte to write
 * @param to_write_length the length of the block of byte
 * 
 * @return the number of bytes written in the file
 */
int write_block_in_file(file_fd_t file_fd, byte* to_write, const short to_write_length);

/**
 * @brief Some const for the close_file function
 */
#define CLOSING_SUCCESS 1
#define ERROR_DURING_CLOSING 0

/**
 * @brief Close a file descriptor
 * 
 * @param file_fd the file descriptor to close
 * 
 * @return A flag describing if closing was successfull
 */
short close_file(file_fd_t file_fd);

byte* serialize_file_block(const byte* file_block, const int file_block_length);
byte* deserialize_file_block(const byte* serialized_file_block, const int file_block_length);

#endif // H_fs_api