#ifndef H_serialization_files
#define H_serialization_files

/* --- IMPORTS --- */
/* Typing */
#include "filesDAO.h"
#include "serialization.h"

#define NUMBER_OF_FIELDS_OF_FILES 4

byte* serialize_file(const File* files);
int total_number_of_byte_of_file(const File* file);
File* deserialize_file(byte* file_as_byte);

#endif

