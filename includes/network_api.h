#ifndef H_network_api
#define H_network_api

/* --- IMPORTS --- */
/* Connection */
#include "connection.h"

/* --- CODE --- */

/**
 * @brief Host a TCP server on the specified port
 * 
 * @param port_num the port number to listen on
 * 
 * @return the corresponding server socket
 */
t_socket_fd host_TCP_server(const short port_num);

/**
 * @brief Connect to a TCP server by specifying it's ip adress and it's port number
 * 
 * @param id_addr the ip address of the server
 * @param port_num the port number of the server
 * 
 * @return the corresponding socket
 */
t_socket_fd connect_to_TCP_server(const char* ip_addr, const short port_num);

/**
 * @brief Host an UDP server on the specified port
 * 
 * @param port_num the port number to listen on
 * 
 * @return the corresponding server socket
 */
t_socket_fd host_UDP_server(const short port_num);

#endif // H_network_api