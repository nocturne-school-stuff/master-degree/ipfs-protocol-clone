#ifndef H_checksum
#define H_checksum

/* --- IMPORTS --- */
/* Serialization */
#include "serialization.h"
/* Hashing */
#include "hash32.h"

/* --- CODE --- */
/**
 * @brief Compute the checksum for a given byte array
 * 
 * @param data the byte array
 * @param data_length the length of the byte array
 * 
 * @return the checksum corresponding to the byte array
 */
hash32_t compute_checksum(byte* data, int data_length);

/**
 * @brief Some const for verifying the validity of a checksum
 */
#define CHECKSUM_VALID 1
#define CHECKSUM_NOT_VALID 0

/**
 * @brief Tells if the checksum for a given byte array is valid
 * 
 * @param data the byte array
 * @param data_length the length of the byte array
 * @param checksum the checksum to test
 * 
 * @return A flag that describe if the checksum correspond to the data (1 if so) 
 */
short verify_checksum(byte* data, int data_length, hash32_t checksum);

#endif // H_uchecksum