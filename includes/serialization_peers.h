#ifndef H_serialization_peers
#define H_serialization_peers

/* --- IMPORTS --- */
/* Typing */
#include "peersDAO.h"
#include "serialization.h"

#define NUMBER_OF_FIELDS_OF_PEER 3

byte* serialize_peer(const Peer* peer);
int total_number_of_byte_of_peer(const Peer* peer);
Peer* deserialize_peer(const byte* peer_as_byte);

#endif

