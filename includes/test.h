#ifndef H_template
#define H_template

/* --- CODE --- */
/**
 * @brief A function that start the test playground, an area to test the local code base
 * 
 * @return A flag describing if the test had the expected result (EXIT_SUCCESS if so)
 */
int start_test_playground();

#endif