#ifndef H_UDPServer
#define H_UDPServer

/* --- IMPORTS --- */
#include "connection.h"
#include "tnfs_UDP_packet.h"

/* --- CODE --- */

/**
 * @brief A dedicated thread function to handle the arrival of tnfs udp packets
 * 
 * @param args 2 argument : the socket fd of the client and the tnfs packet it send
 */
void handle_tnfs_UDP_packet(void* args);

/**
 * @brief Start a local tnfs UDP server
 * 
 * @param port_num the port number to listen on
 * 
 * @return a status code 
 */
int start_UDP_server();

#endif // H_UDPServer