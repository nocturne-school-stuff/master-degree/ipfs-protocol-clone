#ifndef H_deserialization
#define H_deserialization

/** -- IMPORTS --- */
/* Common */
#include <stdlib.h>

/* --- CODE ---*/
/**
 * Represent a byte in memory
 */
typedef u_int8_t byte;

/**
 * @brief Cast a byte to a (signed) char
 * 
 * @param b the byte to convert into a char 
 * 
 * @return the byte as a (signed) char
 */
char byte_to_char(const byte b);

/**
 * @brief Cast a byte array to a char array
 * 
 * @param b the byte array to convert into a char array
 * @param b_length the length of the byte array
 * 
 * @return the byte array as a char array
 */
char* byte_array_to_char_array(const byte* b, const int b_length);

/**
 * @brief Cast a byte array into an integer
 * 
 * @param b the byte array to convert into an integer 
 * 
 * @return the byte array as an integer
 */
int byte_array_to_integer(const byte* b);

#endif // H_deserialization