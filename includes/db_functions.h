#ifndef H_db_functions
#define H_db_functions

#include <sqlite3.h>

/* --- CODE --- */
/**
 * @brief a const used to read the content of a file (here use to read script.sql)
 */ 
#define BUFFER_SIZE 1000

/**
 * @brief a var that contains the local_database.db
 */ 
sqlite3 *db;
/**
 * @brief a var that contains an sql error message
 */ 
extern char *err_msg;

/**
 * @brief a var that contains the content of the script.sql
 */ 
char sql_tables[2046];


/**
 * @brief Read the sql script
 * 
 * @return return 0
 */
int read_sql_file();


/**
 * @brief Open the local_database.db
 
 * @return return 0 if ok else return 1
 */
int open_db();


/**
 * @brief return the result of a sql querry
 * 
 * @param NotUsed a data provided in the 4th argument of sqlite3_exec(); it is often not used
 * @param argc number of columns in the result 
 * @param argv an array of strings representing fields in the row
 * @param azColName an array of strings representing column names
 * 
 * @return return 0 
 */
int callback(void *NotUsed, int argc, char **argv, char **azColName);

#endif
