#ifndef H_ip_resolution
#define H_ip_resolution

/**
 * @brief Resolve the ip address of a node id
 * 
 * @param node_id the node id to resolve
 * 
 * @return the ip address corresponding to the node id
 */
char* get_ip_addr_from_node_id(const char* node_id);

/**
 * @brief Resolve the node id of an ip address
 * 
 * @param ip_addr the ip addr to resolve
 * 
 * @return the node id corresponding to the ip address
 */
char* get_node_id_from_ip_addr(const char* ip_addr);

#endif