#ifndef H_uerror
#define H_uerror

/* --- CODE --- */

/**
 * @brief Print an error message and exit the programm
 * 
 * @param message the error message to print (NULL will print nothing)
 */
void error_exit(char* message);

/**
 * @brief Print an error message
 * 
 * @param message the error message to print
 */
void error_message(char* message);

#endif // H_uerror