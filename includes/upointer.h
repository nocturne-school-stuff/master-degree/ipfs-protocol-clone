#ifndef H_upointer
#define H_upointer

/* --- CODE --- */

/**
 * @brief Create a pointer to an int value
 * 
 * @param value the value referenced by the pointer
 * 
 * @return A pointer to the value
 */
int* int_pointer(int value);

#endif // H_upointer