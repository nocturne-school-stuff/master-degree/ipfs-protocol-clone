#ifndef H_multithreading
#define H_multithreading

/* --- IMPORTS --- */
/* Multithreading */
#include <pthread.h>
/* --- CODE --- */

/**
 * @brief a const that describe an invalid thread identifier (resulting of creation failure)
 */ 
#define INVALID_THREAD_IDENTIFIER -1 
/**
 * @brief some const for the kill_thread function and the wait_for_thread function
 */
#define CANNOT_CANCEL_THREAD 0 
#define THREAD_CANCELLED 1
#define THREAD_FINISHED 1
#define CANNOT_FIND_THREAD -1

/**
 * @brief A number that correspond to a thread
 */
typedef pthread_t thread_identifier;

/**
 * A shortcut for creating a new thread with given args
 * 
 * @param function_ptr A pointer to the function that will start the thread
 * 
 */
thread_identifier create_new_thread(const void* function_ptr, const void* args);

/**
 * @brief A shortcut for creating the dedicated thread for the local TNFS TCP server
 * 
 * @return the thread identifier of the newly created thread
 */
thread_identifier create_TCP_server_thread();

/**
 * @brief A shortcut for creating the dedicated thread for the local TNFS UDP server
 * 
 * @return the thread identifier of the newly created thread
 */
thread_identifier create_UDP_server_thread();

/**
 * @brief A shortcut for creating the dedicated thread for the TNFS client
 * 
 * @return the thread identifier of the newly created thread
 */
thread_identifier create_client_thread();

/**
 * @brief A blocking function that will wait for the thread specified by the thread identifier to finish
 * 
 * @param t_id the thread identifier of the thread to wait
 * 
 * @return A flag that describes if the thread exist or not (1 if so else 0)
 */
short wait_for_thread(thread_identifier t_id);

/**
 * @brief A function that will try to cancel the execution for the thread specified by the thread identifier
 * 
 * @param t_id the thread identifier of the thread to kill
 * 
 * @return A flag that describes if the thread has been successfully killed (1 if so, 0 if not, -1 if the thread doesnt exist)  
 */
short kill_thread(thread_identifier t_id);

/**
 * @brief A mutex can
 */
typedef pthread_mutex_t mutex_t;

/**
 * @brief some const for the lock_mutex and unlock_mutex function
 */
#define MUTEX_LOCKED 1
#define MUTEX_LOCKING_ERROR 0
#define MUTEX_UNLOCKED 1
#define MUTEX_UNLOCKING_ERROR 0

/**
 * @brief Create an initialized mutex
 * 
 * @return The newly created mutex
 */
mutex_t create_mutex();

/**
 * @brief A blocking function that will try to lock a given mutex
 * 
 * @param mutex a pointer to the mutex to lock
 * 
 * @return A flag that describe if the mutex has been successfully locked
 */
short lock_mutex(mutex_t mutex);

/**
 * @brief A blocking function that will try to unlock a given mutex
 * 
 * @param mutex a pointer to the mutex to unlock
 * 
 * @return a flag that describe if the mutex has been successfully unlocked (0 )
 */
short unlock_mutex(mutex_t mutex);

#endif // H_multithreading