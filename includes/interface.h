#ifndef H_interface
#define H_interface

/* --- Imports --- */
/* Common */
#include <stdlib.h>
/* typing */
#include "hash32.h"

/* interface constants and variables*/

#define INVALID_INPUT NULL
#define MAX_INPUT_LENGTH 255
extern const char* home_message;
extern const char* join_network_message;
extern const char* publish_file_message;
extern const char* download_file_message;
extern const char* exit_message;
extern const char* input_error_message;

/**
 * @brief Interface home menu options
 */
typedef enum interface_options {
    JOIN_NETWORK = 1,
    PUBLISH_FILE = 2,
    DOWNLOAD_FILE = 3,
    EXIT = 4
} interface_options;

/**
 * @brief Clear the input buffer (stdin)
 */
void flush_input_buffer();

/**
 * @brief Read a string from stdin
 * 
 * @param length_ptr an integer pointer to keep track of the length read
 * 
 * @return the read string
 */
char* read_next_string(int* length_ptr);

/**
 * @brief Read an int from stdin
 * 
 * @return the read int or 0 if the user input wasnt valid
 */
int read_next_int();

/**
 * @brief Read an hash32_t from stdin
 * 
 * @return the read hash32_t as a string else NULL if an error occured 
 */
hash32_char_t* read_next_hash32();

/**
 * @brief Read an ip address from stdin (format: xxx.xxx.xxx.xxx:pppp)
 * 
 * @return a array containing the ip address at index 0 and the port number at 1
 */
char** read_next_ip_address();

#endif // interface