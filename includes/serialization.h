#ifndef H_serialization
#define H_serialization

/* --- IMPORTS --- */
/* Common */
#include <stdlib.h>

/* --- CODE --- */
/**
 * Represent a byte in memory
 */
typedef u_int8_t byte;

/**
 * @brief Cast a (unsigned) char to a byte
 * 
 * @param c the char to convert into a byte 
 * 
 * @return the char as a byte
 */
byte char_to_byte(const char c);

/**
 * @brief Cast a char array to a byte array
 * 
 * @param s the char array to convert into a byte array
 * @param s_length the length of the char array
 * 
 * @return the char array as a byte array
 */
byte* char_array_to_byte_array(const char* s, int s_lenght);

/**
 * @brief Cast an integer to a byte
 * 
 * @param i the integer to convert into a byte array 
 * 
 * @return the integer as a byte array 
 */
byte* integer_to_byte_array(const int i);

byte* concat_byte_arrays(byte* base, byte* to_concat, const int to_concat_length, int* bytes_moved);

byte* copy_byte_array(const byte* original, const int original_length);

#endif // H_serialization