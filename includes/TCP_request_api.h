#ifndef H_TCP_request_api
#define H_TCP_request_api

/* --- IMPORTS --- */
/* Typing */
#include "connection.h"
#include "hash32.h"

/* --- CODE --- */

#define TCP_REQUEST_FAILED 1
#define TCP_REQUEST_SUCESS 0

/**
 * @brief A shortcut for aborting a TCP request
 * 
 * @param abort_reason the reason of the abortion
 * @param abort_code the abortion code
 * 
 * @return the specified abortion code
 */
short abort_TCP_request(char* abort_reason, short abort_code);

/**
 * @brief Connect and start a request with a remote TCP server
 * 
 * @param ip_address the ip address of the remote TCP server
 * @param port_num the port number of the remote TCP server
 * @param dest_node the node id of the remote TCP server
 * 
 * @return The socket_fd of the connection (INVALID_SOCKET_FD if failed)
 */
t_socket_fd initiate_TCP_request(char* ip_address, int port_num, hash32_t dest_node);

/**
 * @brief Handle the starting of a request with a remote TCP client
 * 
 * @param socket_fd the socket_fd of the connection
 * 
 * @return the node id of the source
 */
hash32_t handle_initiate_TCP_request(t_socket_fd socket_fd);

/**
 * @brief End a request with a remote TCP server
 * 
 * @param socket_fd the socket_fd of the connection
 * @param dest_node the node id of the remote TCP server
 */
void end_TCP_request(t_socket_fd socket_fd, hash32_t dest_node);

/**
 * @brief Handle the end of a request with a remote TCP client
 * 
 * @param socket_fd the socket_fd of the connection
 */
void handle_end_TCP_request(t_socket_fd socket_fd);

/**
 * @brief Perform a file download request
 * 
 * @param file_name the name of the file to download
 * @param file_name_length the length of the name of the file
 * 
 * @return a flag describing the request result
 */
short download_file_request(char* file_name, int file_name_length);

/**
 * @brief Handle a file download request
 * 
 * @param socket_fd the socked used for communicating
 * @param dest_node the id of the node downloading the file
 * @param src_node the id of the node transfering the file
 * @param file_cid the cid of the file to download
 * 
 * @return a flag describing the request result
 */
short handle_download_file_request(t_socket_fd socket_fd, hash32_t dest_node, hash32_t src_node, hash32_t file_cid);

/**
 * @brief Perform a file publish request
 * 
 * @param file_path the path of the file to publish
 * @param file_path_length the length of the file path
 * 
 * @return a flag describing the request result
 */
short file_publishing_request(char* file_path, int file_path_length);

/**
 * @brief Handle a file download request
 * 
 * @param socket_fd the socked used for communicating
 * @param dest_node the id of the node downloading the file
 * @param src_node the id of the node transfering the file
 * @param file_cid the cid of the file to download
 * 
 * @return a flag describing the request result
 */
short handle_file_publishing_request(t_socket_fd socket_fd, hash32_t dest_node, hash32_t src_node, char* file_name, int file_name_length);

/**
 * @brief Perfom a get peers request
 * 
 * @param ip_addr the ip address of the peers to get peers from
 * @param port_num the port_num of the peers to get peers from
 * 
 * @return a flag describing the request result
 */
short get_peers_request(char* ip_addr, int port_num);

/**
 * @brief Handle a get peers request
 * 
 * @param socket_fd the socked used for communicating
 * @param dest_node the id of the node querying the peers
 * 
 * @return a flag describing the request result
 */
short handle_get_peers_request(t_socket_fd socket_fd, hash32_t dest_node);

#endif // H_TCP_request_api