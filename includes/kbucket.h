#ifndef H_kbucket
#define H_kbucket

/* --- IMPORTS --- */
/* Typing */
#include "peersDAO.h"
#include "hash32.h"

/* --- CODE --- */

typedef struct Bucket {
    Peer* peer;
    struct BucketRow* subBuckets[2];
} Bucket;

typedef struct BucketRow {
    unsigned long min_range;
    unsigned long max_range;
    struct Bucket* value;
} BucketRow;

Bucket* init_bucket();
BucketRow* init_bucket_row(unsigned long min_range, unsigned long max_range);
void add_peer_to_bucket_row(BucketRow* bucketRow, Peer* peer);
Peer* find_peer(BucketRow* bucketRow, hash32_t peer_cid);
Peer* find_nearest_peer(BucketRow* bucketRow, hash32_t peer_cid);

#endif // H_kbucket