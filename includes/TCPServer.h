#ifndef H_TCPServer
#define H_TCPServer

/* --- Imports --- */
/* Networking */
#include "connection.h"

/* --- CODE --- */

/**
 * @brief Handle a communication with a client that is connected to the TCP server
 * 
 * @param args 1 argument : the socket_fd of the client
 */
void handle_client(void* args);

/**
 * @brief The closing procedure of the application if the TCP server encounter an error
 * 
 * @param message the error message to display
 */
void error_TCP_server(char* message);

/**
 * @brief Start a local tnfs TCP server
 * 
 * @param port_num the port number to listen on
 * 
 * @return a status code 
 */
int start_TCP_server();

#endif // H_TCPServer