#ifndef H_TCP_procedures
#define H_TCP_procedures

/* --- IMPORTS --- */
/* Typing */
#include "connection.h"
#include "hash32.h"

/* --- CODE --- */

/**
 * @brief Some procedures const
 */ 
#define TCP_PROCEDURE_CONNECTION_ERROR -1
#define FILE_SHARING_ERROR 404
#define TCP_PROCEDURE_SUCESS 0

/**
 * @brief A shortcut for aborting a TCP procedure
 * 
 * @param abort_reason the reason of the aborting
 * @param abort_code the flag describing the abortion
 * 
 * @return will return the specified abort_flag
 */
short abort_TCP_procedure(char* abort_reason, short abort_code);

/**
 * @brief Download a file from a remote TNFS TCP server
 * 
 * @param socket_fd the socked used for communicating
 * @param src_node the id of the node downloading the file
 * @param dest_node the id of the node transfering the file
 * @param file_cid the cid of the file to download
 * @param save_path the path where the file will be saved
 * 
 * @return A flag describing the result of the procedure
 */
short download_file(t_socket_fd socket_fd, hash32_t src_node, hash32_t dest_node, char* save_path);

/**
 * @brief Transfer a file to a remote TNFS TCP server
 * 
 * @param socket_fd the socked used for communicating
 * @param src_node the id of the node transfering the file
 * @param dest_node the id of the node downloading the file
 * @param file_path the cid of the file to transfer
 * 
 * @return A flag describing the result of the procedure
 */
short transfer_file(t_socket_fd socket_fd, hash32_t src_node, hash32_t dest_node, hash32_t file_path);

/**
 * @brief Share the locals peer ids to a remote TNFS TCP Client
 * 
 * @param socket_fd the socked used for communicating
 * @param src_node the id of the node sharing the peers
 * @param dest_node the id of the node querying the peers
 * 
 * @return A flag describing the result of the procedure
 */
short share_peers(t_socket_fd socket_fd, hash32_t src_node, hash32_t dest_node);

#endif // H_TCP_procedures