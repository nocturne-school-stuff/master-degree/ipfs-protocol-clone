#ifndef H_tnfs_TCP_packet
#define H_tnfs_TCP_packet

/* --- IMPORTS --- */
/* Common */
#include <stdlib.h>
/* Serialization */
#include "serialization.h"
/* Packet typing */
#include "tnfs_packet_type.h"
#include "peersDAO.h"
/* Utilitaries */
#include "hash32.h"

/* --- CODE --- */

#define NUMBER_OF_FIELDS_OF_TNFS_TCP_PACKET 8

/**
 * @brief The length (in bytes) of the tnfs_tcp_packet header
 */
extern const int TNFS_TCP_PACKET_HEADER_LENGTH; 
/**
 * @brief The length (in bytes) of the tnfs_tcp_packet footer
 */
extern const int TNFS_TCP_PACKET_FOOTER_LENGTH;

/**
 * @brief A struct that encapsulated all the property that are needed for two node to communicate with each others over a TCP connection
 */
typedef struct t_tnfs_TCP_packet {
    /** 
     * The node id of the recipient
     */
    hash32_t dest_node_id;
    /**
     * The node id of the sender 
     */
    hash32_t src_node_id;
    /**
    /**
     * A flag describing the purpose of the packets
     */
    t_tnfs_packet_type type;
    /**
     * The size of the payload (in bytes)
     */
    int payload_size;
    /**
     * The content carried by the tnfs packets
     */
    void* payload;
    /**
     * The checksum of the payload
     */
    hash32_t checksum;
    /**
     * The number of the packet for a given session
     */
    int packet_number;
    /**
     * The session id the packet
     */
    int session_id;
} t_tnfs_TCP_packet;

/**
 * @brief Copy and encapsulate all the given parameters into a tnfs_TCP_packet. 
 * 
 * @param dest_node_id the node id of the recipient
 * @param src_node_id the node id of the sender
 * @param type A flag describing the purpose of the packets
 * @param payload_size the number of bytes of the payload
 * @param payload the content carried by the tnfs packets
 * @param checksum the checksum of the content
 * @param packet_number the number of the packet for a given session
 * @param session_id the session id of the packet
 * 
 * @return A pointer to the newly created tnfs_TCP_packet
 */
t_tnfs_TCP_packet* encapsulate_tnfs_TCP_packet(const hash32_t dest_node_id, const hash32_t src_node_id, const t_tnfs_packet_type type, const int payload_size, const void* payload, const hash32_t checksum, const int packet_number, const int session_id);

/**
 * @brief Deallocate all the memory taken by a tnfs_TCP_packet
 * 
 * @param tnfs_TCP_packet The tnfs_TCP_packet to destroy
 */
void destroy_tnfs_TCP_packet(t_tnfs_TCP_packet* tnfs_TCP_packet);

/**
 * @brief Cast a tnfs_TCP_packet object to a byte array
 * 
 * @param tnfs_TCP_packet the tnfs_TCP_packet to convert into a byte array
 * 
 * @return a tnfs_TCP_packet as a byte array
 */
byte* serialize_tnfs_TCP_packet(const t_tnfs_TCP_packet* tnfs_TCP_packet);

/**
 * @brief Cast a byte array into a tnfs_TCP_packet object
 * 
 * @param tnfs_TCP_packet_as_bytes the byte array to convert into a tnfs_TCP_packet object
 * 
 * @return a tnfs_TCP_packet object
 */
t_tnfs_TCP_packet* deserialize_tnfs_TCP_packet(byte* tnfs_TCP_packet_as_bytes);

/**
 * @brief Compute the total number of byte of a tnfs_TCP_packet
 * 
 * @param tnfs_TCP_packet the tnfs_TCP_packet that will be used for the computation
 * 
 * @return the total number of byte of the tnfs_TCP_packet
 * 
 */
int total_number_of_byte_of_tnfs_TCP_packet(const t_tnfs_TCP_packet* tnfs_TCP_packet);

/**
 * @brief A utilitary function to rapidly dump all of the properties of a tnfs_TCP_packet
 * 
 * @param tnfs_TCP_packet the tnfs_TCP_packet to print
 */
void print_tnfs_TCP_packet(const t_tnfs_TCP_packet* tnfs_TCP_packet);

/* --- SHORTCUTS --- */

/**
 * @brief A shortcut for creating acknowledgment tnfs_TCP_packet
 * 
 * @param dest_node the destination node
 * @param src_node the source node
 * 
 * @return the newly created acknowledgment tnfs_TCP_packet 
 */
t_tnfs_TCP_packet* create_ACK_tnfs_TCP_packet(const hash32_t dest_node, const hash32_t src_node);

/**
 * @brief A shortcut for creating synchronization tnfs_TCP_packet
 * 
 * @param dest_node the destination node
 * @param src_node the source node
 * 
 * @return the newly created synchronization tnfs_TCP_packet 
 */
t_tnfs_TCP_packet* create_SYN_tnfs_TCP_packet(const hash32_t dest_node, const hash32_t src_node);

/**
 * @brief A shortcut for creating end synchronization tnfs_TCP_packet
 * 
 * @param dest_node the destination node
 * @param src_node the source node
 * 
 * @return the newly created end synchronization tnfs_TCP_packet 
 */
t_tnfs_TCP_packet* create_END_SYN_tnfs_TCP_packet(const hash32_t dest_node, const hash32_t src_node);

/**
 * @brief A shortcut for creating retry tnfs_TCP_packet
 * 
 * @param dest_node the destination node
 * @param src_node the source node
 * @param block_number correspond to the block number of the file to resend
 * 
 * @return the newly created retry tnfs_TCP_packet 
 */
t_tnfs_TCP_packet* create_RETRY_tnfs_TCP_packet(const hash32_t dest_node, const hash32_t src_node, const int block_number);

/**
 * @brief A shortcut for creating error during operation tnfs_TCP_packet
 * 
 * @param dest_node the destination node
 * @param src_node the source node
 * @param error_code the error code of the operation
 * 
 * @return the newly created error during operation  tnfs_TCP_packet 
 */
t_tnfs_TCP_packet* create_ERROR_DURING_OPERATION_tnfs_TCP_packet(const hash32_t dest_node, const hash32_t src_node, const int error_code);

/**
 * @brief A shortcut for creating download tnfs_TCP_packet
 * 
 * @param dest_node the destination node
 * @param src_node the source node
 * @param file_cid the cid of the file to download
 * 
 * @return the newly created download tnfs_TCP_packet 
 */
t_tnfs_TCP_packet* create_DOWNLOAD_tnfs_TCP_packet(const hash32_t dest_node, const hash32_t src_node, const hash32_t file_cid);

/**
 * @brief A shortcut for creating download end tnfs_TCP_packet
 * 
 * @param dest_node the destination node
 * @param src_node the source node
 * 
 * @return the newly created download end tnfs_TCP_packet 
 */
t_tnfs_TCP_packet* create_DOWNLOAD_END_tnfs_TCP_packet(const hash32_t dest_node, const hash32_t src_node);

/**
 * @brief A shortcut for creating publish tnfs_TCP_packet
 * 
 * @param dest_node the destination node
 * @param src_node the source node
 * @param file_name the name of the file
 * @param file_name_length the length of the name of the file
 * 
 * @return the newly created publish tnfs_TCP_packet 
 */
t_tnfs_TCP_packet* create_PUBLISH_tnfs_TCP_packet(const hash32_t dest_node, const hash32_t src_node, char* file_name, int file_name_length);

/**
 * @brief A shortcut for creating publish end tnfs_TCP_packet
 * 
 * @param dest_node the destination node
 * @param src_node the source node
 * 
 * @return the newly created publish end tnfs_TCP_packet 
 */
t_tnfs_TCP_packet* create_PUBLISH_END_tnfs_TCP_packet(const hash32_t dest_node, const hash32_t src_node);

/**
 * @brief A shortcut for creating file block tnfs_TCP_packet
 * 
 * @param dest_node the destination node
 * @param src_node the source node
 * @param block the block to send
 * @param block_length the length of the block
 * 
 * @return the newly created retry tnfs_TCP_packet 
 */
t_tnfs_TCP_packet* create_FILE_BLOCK_tnfs_TCP_packet(const hash32_t dest_node, const hash32_t src_node, byte* block, const int block_length);

/**
 * @brief A shortcut for creating get peers tnfs_TCP_packet
 * 
 * @param dest_node the destination node
 * @param src_node the source node
 * 
 * @return the newly created get peers tnfs_TCP_packet
 */
t_tnfs_TCP_packet* create_GET_PEERS_tnfs_TCP_packet(const hash32_t dest_node, const hash32_t src_node);

/**
 * @brief A shortcut for creating get peers response tnfs_TCP_packet
 * 
 * @param dest_node the destination node
 * @param src_node the source node
 * @param peer a peer
 * 
 * @return the newly created get peers response tnfs_TCP_packet
 */
t_tnfs_TCP_packet* create_TNFS_GET_PEERS_RESPONSE_TCP_packet(const hash32_t dest_node, const hash32_t src_node, Peer* peer);

/**
 * @brief A shortcut for creating get peers end tnfs_TCP_packet
 * 
 * @param dest_node the destination node
 * @param src_node the source node
 * 
 * @return the newly created get peers end tnfs_TCP_packet
 */
t_tnfs_TCP_packet* create_GET_PEERS_END_tnfs_TCP_packet(const hash32_t dest_node, const hash32_t src_node);

#define F_TNFS_GET_PEERS 0x20
#define F_TNFS_GET_PEERS_RESPONSE 0x21
#define F_TNFS_GET_PEERS_END 0x22

#endif // H_tnfs_TCP_packet