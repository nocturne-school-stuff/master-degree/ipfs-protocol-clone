#ifndef H_filesDAO
#define H_filesDAO

/* --- CODE --- */

/**
 * @brief a struct that match the Files table of the database
 */ 
typedef struct File {
     /**
     *  the cid of a file
     */
   char cid[5];
    /**
     *  the name of the file
     */
   char name[64];
     /**
     *  the path where the file is 
     */
   char path[255];
    /**
     *  the id of the hosting peer
     */
   char hosting_peer_id[5];
} File; 


/**
 * @brief Make a sql querry to get all content of the files tables from the local_databse.db

 * @return return a list of file that contains all files of the database
 */
File** select_all_files(int* nb_row); 


/**
 * @brief Make a sql querry to select a file with his cid
 * 
 * @param cid the cid of the file that we want to get
 * 
 * @return return 0 if ok / call callback to return result
 */
File* select_by_cid(char cid[5]);


/**
 * @brief Make a sql querry to insert a new file in the files table of local_databse.db
 * 
 * @param file the new file that we want to insert in the db
 * 
 * @return return 0 if ok 
 */
int insert_file(File *file);


/**
 * @brief Make a sql querry to update a file
 * 
 * @param cid the cid of the file that need to be updated
 * @param file the new file that will replace the old file
 * 
 * @return return 0 if ok
 */
int update_file(char cid[5], File *file);


/**
 * @brief Make a sql querry to delete a file
 * 
 * @param cid the cid of the file that need to be deleted
 * 
 * @return return 0 if ok
 */
int delete_file(char cid[5]);

#endif
