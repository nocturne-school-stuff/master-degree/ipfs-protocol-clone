#ifndef H_local_config
#define H_local_config

/* --- CODE --- */

/**
 * @brief The local config file path
 */ 
#define LOCAL_CONFIG_FILE_PATH "local.conf"

/**
 * @brief Flags for the local config functions
 */
#define LOCAL_CONFIG_LOADED 1
#define COULDNT_LOAD_LOCAL_CONFIG 0
#define LOCAL_CONFIG_CREATION_SUCCESS 1
#define LOCAL_CONFIG_CREATION_FAILURE 0
#define LOCAL_CONFIG_LOADING_BUFFER_LENGTH 32

/**
 * @brief Load the local config into globals
 * 
 * @return A flag describing of the operation successed
 */
short load_local_config();

/**
 * @brief Create a new local config file
 * 
 * @return a flag describing if the operation successed
 */
short create_local_config();

#endif