#ifndef H_peersDAO
#define H_peersDAO

/* --- CODE --- */

/**
 * @brief a struct that match the Peers table of the database
 */ 
typedef struct Peer {
     /**
     *  the id of the peer
     */
   char peer_id[5];
    /**
     *  the ip of the peer
     */
   char peer_ip_address[16];
    /**
     *  the source port of the peer
     */
   int peer_source_port;
} Peer; 


/**
 * @brief Make a sql querry to get all content of the peers tables from the local_databse.db

 * @return return 0 if ok else return 1 / call callback to return the result
 */
Peer** select_all_peers(int* nb_row); 


/**
 * @brief Make a sql querry to select a peer with the peer_id
 * 
 * @param peer_id the cid of the file that we want to get
 * 
 * @return return 0 if ok / call callback to return result
 */
Peer* select_by_peer_id(char peer_id[5]);


/**
 * @brief Make a sql querry to insert a new file in the files table of local_databse.db
 * 
 * @param file the new file that we want to insert in the db
 * 
 * @return return 0 if ok 
 */
int insert_peer(Peer *peer);


/**
 * @brief Make a sql querry to update a peer
 * 
 * @param cid the peer_id of the file that need to be updated
 * @param file the new file that will replace the old file
 * 
 * @return return 0 if ok
 */
int update_peer(char peer_id[5], Peer *peer);


/**
 * @brief Make a sql querry to delete a peer
 * 
 * @param peer_id the peer_id of the file that need to be deleted
 * 
 * @return return 0 if ok
 */
int delete_peer(char peer_id[5]);

#endif
