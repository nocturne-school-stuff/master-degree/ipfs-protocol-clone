#ifndef H_globals
#define H_globals

/* --- IMPORTS --- */
/* Typing */
#include "hash32.h"
#include "multithreading_api.h"
#include "kbucket.h"

/* --- CODE --- */
#define DEFAULT_PORT_NUM 9876
#define DEFAULT_MAXIMUM_NUMBER_OF_CLIENTS 16

/* Return Constants */
#define SUCCESS 0
#define FILE_NOT_CREAT 401
#define FILE_NOT_PUB 402
#define FILE_NOT_FOUND 404
#define DB_ERROR 200
#define PEER_ERROR 300

/* Size Constant */
#define BUFFER_SIZE 500

/* Path Constant */
#define FILE_DIR "../../../file/"

/* Global dedicated to the network */
/**
 * The boolean checking if the peer joined the network
 */
extern int joined;

/**
 * The local pid
 */
extern hash32_t G_local_node_id;
/**
 * The local ip address
 */
extern char* G_local_ip_address;

/* Globals dedicated to multithreading*/
/**
 * The thread identifier of the thread dedicated to the TNFS client
 */
extern thread_identifier G_client_thread;
/**
 * The thread identifier of the thread dedicated to the local TNFS TCP server
 */
extern thread_identifier G_TCP_server_thread;
/**
 * The thread identifier of the thread dedicated to the local TNFS UDP server
 */
extern thread_identifier G_UDP_server_thread;
/**
 * Indicate how many clients are currently connected to the local TNFS server
 */
extern short G_number_of_connected_clients;
/**
 * The maximum number of clients that can simultaneously be connected to the local TNFS server
 */
extern short G_maximum_number_of_client;
/**
 * An array of all the threads identifiers of the clients that are connected to the local TNFS server
 */
extern thread_identifier* G_server_clients_thread;

extern BucketRow* G_global_routing_table; 

extern mutex_t G_DB_mutex;

/* Globals dedicated to the configuration */
/**
 * Indicate if the server is in quiet mode (minimum logs are printed)
 */
extern short G_quiet_mode;
/**
 * Indicate if the server is in debug mode
 */
extern short G_debug_mode;
/**
 * The port number on which the local TNFS server will be listening
 */
extern short G_port_num;
/**
 * Indicate if the TNFS client must be started
 */
extern short G_client_mode;
/**
 * Indicate if the TNFS server must be started
 */
extern short G_server_mode;

#endif // H_globals