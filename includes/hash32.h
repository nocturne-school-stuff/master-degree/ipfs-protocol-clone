#ifndef H_hash32
#define H_hash32

/* --- IMPORTS --- */
/* Common */
#include <stdlib.h>

#define HASH32_LENGTH 32

/**
 * @brief A shortcut for the 32 bits long type
 */
typedef u_int32_t hash32_t;
/**
 * @brief A shortcut for unsigned char 
 */
typedef u_int8_t hash32_char_t;
/**
 * @brief A utilitary function for creating 32 bits long hash
 * 
 * @param to_hash the string to hash
 * @param to_hash_length the string length
 * 
 * @return a 32 bits long hash of the initial string
 */
hash32_t hash32(char* to_hash, const int to_hash_length);

/**
 * @brief A utilitary function for generating a pseudo-random 32 bits long hash
 * 
 * @return a pseudo-random 32 bits long hash
 */
hash32_t random_hash32();

/**
 * @brief convert an 32 bits long value into a string
 * 
 * @param value the value to convert
 * 
 * @return a string representation of the value
 */
hash32_char_t* hash32_to_string(hash32_t value);

/**
 * @brief convert a string of length 4 into a 32 bits long value
 * 
 * @param value_as_string the value to convert
 * 
 * @return a 32 bits long representation of the string
 */
hash32_t string_to_hash32(hash32_char_t value_as_string[5]);

/**
 * @brief a utilitary function to print a hash
 * 
 * @param hash the hash to print
 */
void print_hash(hash32_t hash);

/**
 * @brief a utilitary function to print a hash
 * 
 * @param hash_as_string the hash to print as a string
 */
void print_hash_string(hash32_char_t hash_as_string[5]);

unsigned long xor_metric(hash32_t hash_1, hash32_t hash_2);

#endif