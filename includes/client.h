#ifndef H_client
#define H_client

/* --- CODE --- */

/**
 * @brief Handle a communication with a remote server
 * 
 * @param args 1 argument : the socket_fd of the server
 */
void handle_operation(void* args);

/**
 * @brief The closing procedure of the application if the client encounter an error
 * 
 * @param message the error message to display
 */
void error_client(char* message);

/**
 * @brief Start a tnfs client
 * 
 * @return a status code 
 */
int start_client();

#endif // H_client