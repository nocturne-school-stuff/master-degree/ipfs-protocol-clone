#ifndef H_transmission
#define H_transmission

/* --- IMPORTS --- */
/* Typing */
#include "connection.h"
#include "tnfs_TCP_packet.h"
#include "tnfs_UDP_packet.h"
#include <sys/select.h>
/* --- CODE --- */

/**
 * @brief A const for the maximum time for a request to be in a pending state
 */
#define REQUEST_TIMEOUT_DELAY 1000 * 30

/**
 * @brief Some const flags for the result of a request
 */
#define REQUEST_SUCESS 200
#define SENDING_ERROR 204
#define READING_ERROR 205
#define REQUEST_CORRUPTED 400
#define REQUEST_TIMEOUT 408
#define REQUEST_RETRY 449
#define RECEPIENT_ERROR 500

/**
 * @brief Send a tnfs_packet over a TCP connection
 * 
 * @param TCP_socket_fd The TCP client socket
 * @param tnfs_TCP_packet the tnfs TCP packet to send
 * 
 * @return A flag describing the result of the request
 */
short send_tnfs_TCP_packet(t_socket_fd TCP_socket_fd, t_tnfs_TCP_packet* tnfs_TCP_packet);

/**
 * @brief read a tnfs_packet over a TCP connection
 * 
 * @param TCP_socket_fd The TCP client socket
 * @param tnfs_TCP_packet A pointer of pointer to a TCP packet to initialize
 *
 * @return A flag describing the result of the request
 */
short read_tnfs_TCP_packet(t_socket_fd TCP_socket_fd, t_tnfs_TCP_packet** ptr_tnfs_TCP_packet);

/**
 * @brief Send a tnfs_packet throught an UDP socket
 * 
 * @param ip_address The ip address of the remote UDP server
 * @param port_num the port number of the remote UDP server
 * @param tnfs_UDP_packet the tnfs UDP packet to send
 * 
 * @return A flag describing the result of the request
 */
short send_tnfs_UDP_packet(char* ip_address, int port_num, t_tnfs_UDP_packet* tnfs_UDP_packet);

/**
 * @brief read a tnfs_packet throught an UDP socket
 * 
 * @param UDP_socket_fd The UDP client socket
 * @param client_socket_fd a pointer for initializing a client socket
 * @param tnfs_UDP_packet A pointer to an UDP packet to initialize
 * 
 * @return A flag describing the result of the request
 */
short read_tnfs_UDP_packet(t_socket_fd UDP_socket_fd, t_tnfs_UDP_packet** ptr_tnfs_UDP_packet);

#endif // H_transmission