#ifndef H_connection
#define H_connection

/* --- IMPORTS --- */
/* Networking */
#include <netinet/in.h>
#include <sys/socket.h>

/**
 * @brief a const that describes if a socket file descriptor is invalid
 */
#define INVALID_SOCKET_FD -1

/**
 * @brief a const that describes the maximum transmission unit aka the maximum size (in byte) of the content that can be send over the network
 */
#define MTU 1500

#define TCP SOCK_STREAM
#define UDP SOCK_DGRAM

/* --- CODE --- */
/**
 * Represent a socket file descriptor
 */
typedef int t_socket_fd;

/**
 * @brief Represent a socket configuration
 */
typedef struct sockaddr_in t_socket_config;

/**
 * @brief Get a valid socket configuration for a TCP communication
 * 
 * @param dest_addr the ip address of the machine to connect to (NULL if it is a hosting configuration)
 * @param port_num the port number on which to connect to (Or the one which will be listening)
 * 
 * @return A socket_config struct ready to use
 */
t_socket_config* get_socket_config_for(const char* dest_addr, const short port_num);

/**
 * @brief Get a valid server socket configuration for hosting a TCP communication (based on get_socket_config_for)
 * 
 * @param port_num the port number which will be listening
 * 
 * @return A socket_config struct for hosting ready to use
 */
t_socket_config* get_server_config_on(const short port_num);

/**
 * @brief Create a raw socket (not initialized with any configuration)
 * 
 * @param socket_protocol the protocol of the socket (TCP or UDP)
 * 
 * @return A raw socket file descriptor
 */
t_socket_fd create_raw_tcp_socket(const short socket_protocol);

/**
 * @brief Create a valid TCP server sided socket for the given configuration
 * 
 * @param configuration the socket configuration
 * @param socket_protocol the protocol of the socket (TCP or UDP)
 * 
 * @return a socket file descriptor
 */
t_socket_fd create_server_socket(t_socket_config* configuration, const short socket_protocol);

/**
 * @brief Create a valid TCP client sided socket for the given configuration
 * 
 * @param configuration the socket configuration
 * @param socket_protocol the protocol of the socket (TCP or UDP)
 * 
 * @return a socket file descriptor
 */
t_socket_fd create_client_socket(t_socket_config* configuration, const short socket_protocol);

/**
 * @brief Some const for the close_socket_connection function
 */
#define SOCKET_CLOSED 1
#define ERROR_CLOSING_SOCKET 0

/**
 * @brief Close the connection held by a socket
 * 
 * @param socket_fd the socket to close
 */
short close_socket(t_socket_fd socket_fd);

#endif // H_connection