#ifndef H_timeout
#define H_timeout

/* --- IMPORTS --- */
/* Time */
#include <time.h>

/**
 * @brief shortcuts for the return value of the is_timeout function
 */
#define TIMEOUT 1
#define NO_TIMEOUT 0

/**
 * @brief Compute if a delay has been reached from a start time
 * 
 * @param start_time the time when the timeout was started
 * @param delay_in_ms the duration of the timeout
 * 
 * @return 1 if the timeout has been reached else 0
 */
int is_timeout(const time_t start_time, time_t delay_in_ms);

#endif