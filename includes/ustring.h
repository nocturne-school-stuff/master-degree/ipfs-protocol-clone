#ifndef H_ustring
#define H_ustring

/* --- CODE --- */

/**
 * @brief Create an exact copy of a string
 * 
 * @param to_clone the string to clone
 * @param to_clone_length the length of the sting
 * 
 * @return A copy of the original string
 */
char* clone_str(const char* to_clone, const int to_clone_lenght);

/**
 * @brief Some const that describe if two strings are equals
 */
#define STRINGS_EQUALS 1
#define STRINGS_NOT_EQUALS 0
/**
 * @brief Tells if two strings are equals
 * 
 * @param str_1 the first string to compare
 * @param str_2 the second string to compare
 * 
 * @return A flag that describe if the two string are equals (1 if so)
 */
short str_equals(const char* str_1, const char* str_2);

/**
 * @brief Cast an integer to a string
 * 
 * @param i the integer to cast
 * @param length_ptr a pointer to an int to keep track of the string length
 * 
 * @return the integer as a string
 */
char* integer_to_string(const int i, int* length_ptr);

char* str_concat(const char* str_1, const char* str_2);

char** str_split(const char* str, int total_length, const char* delimeter);

char* concat_full_ip_addr(char* ip_addr, int port_num);

#endif // H_ustring