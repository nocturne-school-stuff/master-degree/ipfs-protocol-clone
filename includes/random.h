#ifndef H_random
#define H_random

/* --- IMPORTS --- */
/* Common */
#include <stdlib.h>
/* Utilitaries */
#include <time.h>
/* --- CODE --- */

/**
 * @brief A function that will initialize the pseudo-random generator seed
 */
void init_random_seed();

/**
 * @brief Generate a random integer between min and max both included
 * 
 * @param min the minimum integer value that can be generated
 * @param max the maximum integer value that can be generated
 * 
 * @return  a random integer
 */
int random_int(int min, int max);

/**
 * @brief Generate a random char
 * 
 * @return A random char
 */
char random_char();

/**
 * @brief Generate a random 32 bits long value as an integer
 * 
 * @param length_ptr a pointer to save the length of the newly generated string
 * @param min_length  the minimum length of the string
 * @param max_length the maximum length of the string
 * 
 * @return A random string
 */
char* random_string(int* length_ptr, const int min_length, const int max_length);

#endif