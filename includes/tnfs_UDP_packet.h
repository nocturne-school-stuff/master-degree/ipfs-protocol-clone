#ifndef H_tnfs_UDP_packet
#define H_tnfs_UDP_packet

/* --- IMPORTS --- */
/* Common */
#include <stdlib.h>
/* Serialization */
#include "serialization.h"
/* Packet typing */
#include "tnfs_packet_type.h"
/* Utilitaries */
#include "hash32.h"

/* --- CODE --- */
#define NUMBER_OF_FIELDS_OF_TNFS_UDP_PACKET 5

/**
 * @brief The length (in bytes) of the tnfs_udp_packet header
 */
extern const int TNFS_UDP_PACKET_HEADER_LENGTH; 
/**
 * @brief The length (in bytes) of the tnfs_udp_packet footer
 */
extern const int TNFS_UDP_PACKET_FOOTER_LENGTH;

/**
 * @brief A struct that encapsulated all the property that are needed for a node to send a packet to another one via a UDP socket
 */
typedef struct t_tnfs_UDP_packet {
    /**
     * The node id of the recipient
     */
    hash32_t dest_node_id;
    /**
     * The node id of the sender 
     */
    hash32_t src_node_id;
    /**
     * A flag describing the purpose of the packets
     */
    t_tnfs_packet_type type;
    /**
     * The size of the payload (in bytes)
     */
    int payload_size;
    /**
     * The content carried by the tnfs packets
     */
    void* payload;
} t_tnfs_UDP_packet;

/**
 * @brief Copy and encapsulate all the given parameters into a tnfs_UDP_packet. 
 * 
 * @param src_node_id the node id of the sender
 * @param dest_node_id the node id of the recipient
 * @param type A flag describing the purpose of the packets
 * @param payload_size the number of bytes of the payload
 * @param payload the content carried by the tnfs_UDP_packets
 * 
 * @return A pointer to the newly created tnfs_UDP_packet
 */
t_tnfs_UDP_packet* encapsulate_tnfs_UDP_packet(const hash32_t dest_node_id, const hash32_t src_node_id, const t_tnfs_packet_type type, const int payload_size, const void* payload);

/**
 * @brief  Deallocate all the memory taken by a tnfs_UDP_packet
 * 
 * @param tnfs_UDP_packet The tnfs_UDP_packet to destroy
 */
void destroy_tnfs_UDP_packet(t_tnfs_UDP_packet* tnfs_UDP_packet);

/**
 * @brief Cast a tnfs_UDP_packet object to a byte array
 * 
 * @param tnfs_UDP_packet the tnfs_UDP_packet to convert into a byte array
 * 
 * @return a tnfs_UDP_packet as a byte array
 */
byte* serialize_tnfs_UDP_packet(const t_tnfs_UDP_packet* tnfs_UDP_packet);

/**
 * @brief Cast a byte array into a tnfs_UDP_packet object
 * 
 * @param tnfs_UDP_packet_as_bytes the byte array to convert into a tnfs_UDP_packet object
 * 
 * @return a tnfs_UDP_packet object
 */
t_tnfs_UDP_packet* deserialize_tnfs_UDP_packet(byte* tnfs_UDP_packet_as_bytes);

/**
 * @brief Compute the total number of byte of a tnfs_UDP_packet
 * 
 * @param tnfs_UDP_packet the tnfs_UDP_packet that will be used for the computation
 * 
 * @return the total number of byte of the tnfs_UDP_packet
 * 
 */
int total_number_of_byte_of_tnfs_UDP_packet(const t_tnfs_UDP_packet* tnfs_UDP_packet);

/**
 * @brief A utilitary function to rapidly dump all of the properties of a tnfs_UDP_packet
 * 
 * @param tnfs_UDP_packet the tnfs_UDP_packet to print
 */
void print_tnfs_UDP_packet(const t_tnfs_UDP_packet* tnfs_UDP_packet);

/* --- SHORTCUTS --- */

/**
 * @brief A shortcut for creating ping tnfs_UDP_packet
 * 
 * @param dest_node the destination node
 * @param src_node the source node
 * @param full_ip_addr an ip address suffixed by a port number (format: x.x.x.x:p)
 * 
 * @return the newly created ping tnfs_TCP_packet 
 */
t_tnfs_UDP_packet* create_PING_tnfs_UDP_packet(hash32_t dest_node, hash32_t src_node, char* full_ip_addr);

/**
 * @brief A shortcut for creating ping response tnfs_UDP_packet
 * 
 * @param dest_node the destination node
 * @param src_node the source node
 * @param full_ip_addr an ip address suffixed by a port number (format: x.x.x.x:p)
 * 
 * @return the newly created ping response tnfs_TCP_packet 
 */
t_tnfs_UDP_packet* create_PING_RESPONSE_tnfs_UDP_packet(hash32_t dest_node, hash32_t src_node, char* full_ip_addr);

/**
 * @brief A shortcut for creating file find response tnfs_UDP_packet
 * 
 * @param dest_node the destination node
 * @param src_node the source node
 * @param file_cid the cid of the file
 * 
 * @return the newly created ping response tnfs_TCP_packet 
 */
t_tnfs_UDP_packet* create_FIND_FILE_tnfs_UDP_packet(hash32_t dest_node, hash32_t src_node, hash32_t file_cid);

/**
 * @brief A shortcut for creating file find response tnfs_UDP_packet
 * 
 * @param dest_node the destination node
 * @param src_node the source node
 * @param file_cid the cid of the file
 * 
 * @return the newly created ping response tnfs_TCP_packet 
 */
t_tnfs_UDP_packet* create_FIND_FILE_RESPONSE_tnfs_UDP_packet(hash32_t dest_node, hash32_t src_node, hash32_t file_cid);

#endif // H_tnfs_UDP_packet