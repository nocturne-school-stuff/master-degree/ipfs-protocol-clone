# PPII-TNFS-2022-GRP3

Composition du groupe :
* Alexandra DEMSKI
* Rémi MARSAL
* Ajdin TOPALOVIC

## Installation

```
Prérequis : gcc et make.
```

### Installation des dépendances

```
sudo make install
```

### Compilation du projet

```
make
```

## Lancement du projet

Pour lancer une instance de TNFS : 
```
make run
```

Pour lancer un serveur de bootstrap :
```
make bootstrap
```